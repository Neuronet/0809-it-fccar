package cn.itsource.fallback;

import cn.itsource.constants.GlobalExceptionCode;
import cn.itsource.feign.LoginApi;
import cn.itsource.pojo.dto.LoginDto;
import cn.itsource.result.R;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class LoginApiFallbackFactory implements FallbackFactory<LoginApi> {
    @Override
    public LoginApi create(Throwable cause) {
        cause.printStackTrace();
        return new LoginApi() {
            @Override
            public R saveLogin(LoginDto loginDto) {
                return R.error(GlobalExceptionCode.SERVICE_ERROR);
            }
        };
    }
}

package cn.itsource.feign;

import cn.itsource.fallback.LoginApiFallbackFactory;
import cn.itsource.pojo.dto.LoginDto;
import cn.itsource.result.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "it-fccar-service-uaa", contextId = "loginApi", fallbackFactory = LoginApiFallbackFactory.class)
public interface LoginApi {

    @PostMapping("/login/saveLogin")
    R saveLogin(@RequestBody LoginDto loginDto);

}

package cn.itsource.pojo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class LoginDto {

    private Long id;

    private Integer type;

    private String openId;

    private String phone;

}

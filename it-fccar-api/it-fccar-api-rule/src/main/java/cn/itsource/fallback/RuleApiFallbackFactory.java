package cn.itsource.fallback;

import cn.itsource.constants.GlobalExceptionCode;
import cn.itsource.pojo.dto.MileagePricingDto;
import cn.itsource.feign.RuleApi;
import cn.itsource.pojo.dto.MileagePricingResultDto;
import cn.itsource.result.R;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class RuleApiFallbackFactory implements FallbackFactory<RuleApi> {
    @Override
    public RuleApi create(Throwable cause) {
        cause.printStackTrace();
        return new RuleApi() {
            @Override
            public R<MileagePricingResultDto> mileagePricing(MileagePricingDto mileagePricingDto) {
                return R.error(GlobalExceptionCode.SERVICE_ERROR);
            }
        };
    }
}

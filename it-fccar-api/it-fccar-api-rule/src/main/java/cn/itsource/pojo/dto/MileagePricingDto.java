package cn.itsource.pojo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Accessors(chain = true)
public class MileagePricingDto {

    private Date pricingTime;

    private BigDecimal mileage;

    private Integer waitingMinute;

}

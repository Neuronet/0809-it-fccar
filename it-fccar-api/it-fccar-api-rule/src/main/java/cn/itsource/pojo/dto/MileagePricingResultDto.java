package cn.itsource.pojo.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class MileagePricingResultDto {

    // 订单总金额
    private BigDecimal realOrderAmount;

    // 基础里程（公里）
    private BigDecimal baseMileage;

    // 基础里程价格
    private BigDecimal baseMileageAmount;

    // 超出基础里程的价格
    private BigDecimal exceedBaseMileageAmount = BigDecimal.ZERO;

    // 里程费
    private BigDecimal mileageAmount;

    // 免费等待乘客分钟数
    private Integer freeBaseWaitingMinute;

    // 返程费免费公里数
    private BigDecimal freeBaseReturnMileage;

    // 超出基础里程部分每公里收取的费用
    private BigDecimal exceedBaseReturnEveryKmAmount = BigDecimal.ZERO;

    // 返程费
    private BigDecimal returnAmont;

    // 等时超出免费每分钟收费
    private BigDecimal exceedEveryMinuteAmount;

    // 等时费用
    private BigDecimal waitingAmount;

}

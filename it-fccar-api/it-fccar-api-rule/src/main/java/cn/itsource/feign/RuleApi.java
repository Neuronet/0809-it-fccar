package cn.itsource.feign;

import cn.itsource.pojo.dto.MileagePricingDto;
import cn.itsource.fallback.RuleApiFallbackFactory;
import cn.itsource.pojo.dto.MileagePricingResultDto;
import cn.itsource.result.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "it-fccar-service-rule", contextId = "ruleApi", fallbackFactory = RuleApiFallbackFactory.class)
public interface RuleApi {

    @PostMapping("/rule/mileagePricing")
    R<MileagePricingResultDto> mileagePricing(@RequestBody MileagePricingDto mileagePricingDto);

}

package cn.itsource.feign;

import cn.itsource.fallback.AppDriverApiFallbackFactory;
import cn.itsource.pojo.domain.DriverPoint;
import cn.itsource.result.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(value = "it-fccar-service-hadoop",path = "/app/hadoop/driver", contextId = "appDriverApi", fallbackFactory = AppDriverApiFallbackFactory.class)
public interface AppDriverApi {

    @GetMapping("/locations/{orderNo}")
    R<List<DriverPoint>> getLocationsByOrderNo(@PathVariable("orderNo") String orderNo);

}

package cn.itsource.pojo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DriverPoint {

    private Long id;
    private String latitude;
    private String longitude;
    private Long driverId;
    private String orderNo;

}

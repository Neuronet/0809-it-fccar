package cn.itsource.fallback;

import cn.itsource.constants.GlobalExceptionCode;
import cn.itsource.feign.AppDriverApi;
import cn.itsource.pojo.domain.DriverPoint;
import cn.itsource.result.R;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AppDriverApiFallbackFactory implements FallbackFactory<AppDriverApi> {
    @Override
    public AppDriverApi create(Throwable cause) {
        cause.printStackTrace();
        return new AppDriverApi() {
            @Override
            public R<List<DriverPoint>> getLocationsByOrderNo(String orderNo) {
                return R.error(GlobalExceptionCode.SERVICE_ERROR);
            }
        };
    }
}

package cn.itsource.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class CreateCustomerFineDto {

    private Long customerId;
    private Long orderId;
    private BigDecimal amount;
    private String remark;

}

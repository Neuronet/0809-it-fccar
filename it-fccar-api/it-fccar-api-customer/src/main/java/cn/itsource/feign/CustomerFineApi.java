package cn.itsource.feign;

import cn.itsource.dto.CreateCustomerFineDto;
import cn.itsource.result.R;
import cn.itsource.fallback.CustomerFineApiFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "it-fccar-service-customer", contextId = "customerFineApi", fallbackFactory = CustomerFineApiFallbackFactory.class)
public interface CustomerFineApi {

    @PostMapping("/customer/createCustomerFine")
    R createCustomerFine(@RequestBody CreateCustomerFineDto createCustomerFineDto);

}

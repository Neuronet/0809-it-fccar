package cn.itsource.fallback;

import cn.itsource.constants.GlobalExceptionCode;
import cn.itsource.dto.CreateCustomerFineDto;
import cn.itsource.feign.CustomerFineApi;
import cn.itsource.result.R;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class CustomerFineApiFallbackFactory implements FallbackFactory<CustomerFineApi> {
    @Override
    public CustomerFineApi create(Throwable cause) {
        cause.printStackTrace();
        return new CustomerFineApi() {

            @Override
            public R createCustomerFine(CreateCustomerFineDto createCustomerFineDto) {
                return R.error(GlobalExceptionCode.SERVICE_ERROR);
            }
        };
    }
}

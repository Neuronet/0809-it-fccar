package cn.itsource.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * feign请求拦截器
 */
public class FeignTokenInterceptor implements RequestInterceptor {

    @Value("${sa-token.token-name}")
    private String tokenName;

    /**
     * 每次发feign请求之前会执行
     * @param requestTemplate：下一次请求的请求对象
     * HttpServletRequest：Request，请求的header到后端之后就放在此对象中
     * HttpServletResponse：Response
     *
     */
    @Override
    public void apply(RequestTemplate requestTemplate) {
        // 1.拿到上一次请求的token
        // 1.1.获取上一次请求的request
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        // 1.2.从上一次请求的request中的header中拿到token
        String tokenValue = request.getHeader(tokenName);

        // 2.把拿到的token设置到下一次请求的header中
        requestTemplate.header(tokenName, tokenValue);
    }
}

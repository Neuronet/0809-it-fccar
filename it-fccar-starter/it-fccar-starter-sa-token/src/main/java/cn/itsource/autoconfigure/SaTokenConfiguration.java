package cn.itsource.autoconfigure;

import cn.itsource.config.SaTokenConfig;
import cn.itsource.config.StpInterfaceImpl;
import cn.itsource.interceptor.FeignTokenInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;


@Configuration
public class SaTokenConfiguration {

    @Bean
    public SaTokenConfig saTokenConfig(){
        return new SaTokenConfig();
    }

    @Bean
    public StpInterfaceImpl stpInterface(RedisTemplate redisTemplate){
        return new StpInterfaceImpl(redisTemplate);
    }

    @Bean
    public FeignTokenInterceptor feignTokenInterceptor(){
        return new FeignTokenInterceptor();
    }
}

package cn.itsource.config;

import cn.dev33.satoken.stp.StpInterface;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 自定义权限加载接口实现类
 */
public class StpInterfaceImpl implements StpInterface {

    @Value("${sa-token.permission-key-template}")
    private String permissionKeyTemplate;

    private RedisTemplate redisTemplate;

    public StpInterfaceImpl(RedisTemplate redisTemplate){
        this.redisTemplate = redisTemplate;
    }

    /**
     * 返回一个账号所拥有的权限码集合 
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        // 1.获取到用户权限标识在Redis中的key
        String permissionKey = String.format(permissionKeyTemplate, loginId);
        // 2.根据key从Redis中获取到用户的权限Sn编码集合
        Object permissionSnsObj = redisTemplate.opsForValue().get(permissionKey);
        // 3.如果是乘客或者是司机确实没有权限，那么响应空集合
        if (ObjectUtils.isEmpty(permissionSnsObj)){
            return new ArrayList<>();
        }
        // 4.如果是后台管理人员有权限，那么将Obj对象转换为权限Sn编码集合
        List<String> permissionSns = JSONObject.parseArray(JSONObject.toJSONString(permissionSnsObj), String.class);
        return permissionSns;
    }

    /**
     * 返回一个账号所拥有的角色标识集合 (权限与角色可分开校验)
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        // 本 list 仅做模拟，实际项目中要根据具体业务逻辑来查询角色
        return new ArrayList<String>();
    }

}

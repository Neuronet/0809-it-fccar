package cn.itsource.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "cos")
public class CosProperties {

    private String accessKey;
    private String secretKey;
    private String regionName;
    private String bucketName;
    private String folder;
    private String filePathTemplate;

}

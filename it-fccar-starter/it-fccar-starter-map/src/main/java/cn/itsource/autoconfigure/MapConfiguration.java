package cn.itsource.autoconfigure;

import cn.itsource.template.TxMapTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapConfiguration {

    @Bean
    public TxMapTemplate txMapTemplate(){
        return new TxMapTemplate();
    }

}

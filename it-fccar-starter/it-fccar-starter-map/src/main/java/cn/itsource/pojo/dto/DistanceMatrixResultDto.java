package cn.itsource.pojo.dto;

import lombok.Data;

@Data
public class DistanceMatrixResultDto {

    // 距离
    public Integer distance;

    // 时间
    public Integer duration;

}

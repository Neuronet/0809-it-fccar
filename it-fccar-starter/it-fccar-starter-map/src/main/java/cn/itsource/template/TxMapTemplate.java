package cn.itsource.template;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.itsource.pojo.domain.Point;
import cn.itsource.pojo.dto.DistanceMatrixResultDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.Assert;

public class TxMapTemplate {

    @Value("${fccar.tx-map.distance-matrix-get-url-template}")
    private String distanceMatrixGetUrlTemplate;

    public DistanceMatrixResultDto getDistanceMatrix(String mode, Point from, Point to){
        String fromStr = from.getLatitude() + "," + from.getLongitude();
        String toStr = to.getLatitude() + "," + to.getLongitude();
        String url = String.format(distanceMatrixGetUrlTemplate, mode, fromStr, toStr);
        String resultJsonStr = HttpUtil.get(url);

        // 1.解析JSON字符串为JSON对象
        JSONObject resultJsonObj = JSONUtil.toBean(resultJsonStr, JSONObject.class);
        // 2.获取到状态，判断是否成功
        Integer status = resultJsonObj.getInt("status");
        Assert.isTrue(ObjectUtil.isNotEmpty(status) && status.equals(0), "返回结果异常！");

        // 3.获取数据结果
        JSONObject resJsonObj = resultJsonObj.getJSONObject("result");
        JSONArray rows = resJsonObj.getJSONArray("rows");
        JSONObject rowsZeroth = rows.getJSONObject(0);
        JSONArray elements = rowsZeroth.getJSONArray("elements");
        JSONObject distanceMatrixResult = elements.getJSONObject(0);
        DistanceMatrixResultDto distanceMatrixResultDto = JSONUtil.toBean(distanceMatrixResult, DistanceMatrixResultDto.class);
        return distanceMatrixResultDto;
    }

}

package cn.itsource.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@ConfigurationProperties("fccar.wechat")
public class WechatProperties {

    private String appId;

    private String secret;

    private String openIdUrlTemplate;

    private String accessTokenUrlTemplate;

    private String codeToPhoneUrlTemplate;

}

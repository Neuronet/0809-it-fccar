package cn.itsource.dto;

import lombok.Data;

@Data
public class OpenIdResultDto {

    private String session_key;

    private String openid;

}

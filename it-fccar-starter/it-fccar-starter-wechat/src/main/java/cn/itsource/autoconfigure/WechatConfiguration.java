package cn.itsource.autoconfigure;

import cn.itsource.properties.WechatProperties;
import cn.itsource.template.WechatTemplate;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
// 把properties配置类交给spring管理
@EnableConfigurationProperties({WechatProperties.class})
public class WechatConfiguration {



    /**
     * 将WechatTemplate交给spring管理
     * @param wechatProperties：在类上通过@EnableConfigurationProperties交给spring管理了，参数传递可以直接从
     *                        Spring容器中获取
     * @return
     */
    @Bean
    public WechatTemplate wechatTemplate(WechatProperties wechatProperties){
        return new WechatTemplate(wechatProperties);
    }

}

package cn.itsource.template;

import cn.hutool.http.HttpUtil;
import cn.itsource.dto.OpenIdResultDto;
import cn.itsource.properties.WechatProperties;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.Assert;

public class WechatTemplate {

    private WechatProperties wechatProperties;

    public WechatTemplate( WechatProperties wechatProperties){
        this.wechatProperties = wechatProperties;
    }

    /**
     * 根据code获取openId
     * @return
     */
    public String code2OpenId(String openIdCode){
        String url = String.format(wechatProperties.getOpenIdUrlTemplate(), wechatProperties.getAppId(), wechatProperties.getSecret(), openIdCode);
        String resultJson = HttpUtil.get(url);
        Assert.isTrue(!resultJson.contains("errCode"), "获取OpenId失败!");
        OpenIdResultDto openIdResultDto = JSONObject.parseObject(resultJson, OpenIdResultDto.class);
        return openIdResultDto.getOpenid();
    }

    /**
     * 根据phoneCode获取手机号
     * @param phoneCode
     * @return
     */
    public String code2Phone(String phoneCode) {
        // 1.获取accessToken
        String url = String.format(wechatProperties.getAccessTokenUrlTemplate(), wechatProperties.getAppId(), wechatProperties.getSecret());
        String resultJsonStr = HttpUtil.get(url);
        Assert.isTrue(!resultJsonStr.contains("errCode"), "获取accessToken失败！");
        // 企业里面接收参数的三种方式：1.封装dto接收参数  2.封装成map  3.封装成JsonObject-也是一个key/value结构
        JSONObject jsonObject = JSONObject.parseObject(resultJsonStr);
        String accessToken = jsonObject.getString("access_token");

        // 2.根据accessToken + phoneCode 获取用户手机号
        String phoneUrl = String.format(wechatProperties.getCodeToPhoneUrlTemplate(), accessToken);
        JSONObject param = new JSONObject();
        param.put("code", phoneCode);
        String phoneResultJsonStr = HttpUtil.post(phoneUrl, JSONObject.toJSONString(param));

        Assert.isTrue(!phoneResultJsonStr.contains("errCode"), "获取手机号失败！");
        JSONObject phoneResultJsonObj = JSONObject.parseObject(phoneResultJsonStr);
        String phoneInfo = phoneResultJsonObj.getString("phone_info");
        JSONObject phoneInfoJsonObj = JSONObject.parseObject(phoneInfo);
        return phoneInfoJsonObj.getString("phoneNumber");
    }

}

package cn.itsource.autoconfigure;

import cn.itsource.pojo.properties.GeoSettingProperties;
import cn.itsource.template.LoginTemplate;
import cn.itsource.template.RedisGeoTemplate;
import com.alibaba.fastjson.support.spring.GenericFastJsonRedisSerializer;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;


@Configuration
@EnableConfigurationProperties({GeoSettingProperties.class})
public class RedisConfiguration {

    /**
     * 使用JSON进行序列化
     * @return
     */
    @Bean
    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        // 1.创建一个RedisTemplate对象
        RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();
        // 2.设置连接工厂
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        // 3.创建JSON格式序列化对象
        GenericFastJsonRedisSerializer serializer = new GenericFastJsonRedisSerializer();
        // 4.key的序列化
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        // 5.value的序列化
        redisTemplate.setValueSerializer(serializer);
        // 6.hash结构key的序列化
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        // 7.hash结构value的序列化
        redisTemplate.setHashValueSerializer(serializer);
        return redisTemplate;
    }

    @Bean
    public LoginTemplate loginTemplate(RedisTemplate redisTemplate){
        return new LoginTemplate(redisTemplate);
    }

    @Bean
    public RedisGeoTemplate redisGeoTemplate(RedisTemplate redisTemplate, GeoSettingProperties geoSettingProperties){
        return new RedisGeoTemplate(redisTemplate, geoSettingProperties);
    }

    //RedissonClient客户端，用作分布式锁
    @Bean
    public RedissonClient redissonClient(RedisProperties redisProperties){
        Config config = new Config();
        config.useSingleServer().setAddress("redis://"+redisProperties.getHost()+":"+ redisProperties.getPort()).setPassword(redisProperties.getPassword());
        return Redisson.create(config);
    }

}

package cn.itsource.pojo.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "fccar.geo")
public class GeoSettingProperties {

    private String radius;

    private String limit;

}

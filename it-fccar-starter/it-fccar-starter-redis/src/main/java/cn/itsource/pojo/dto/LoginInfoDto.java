package cn.itsource.pojo.dto;

import lombok.Data;

@Data
public class LoginInfoDto {

    private String username;

    private Integer type;

    private String avatar;

    private Boolean admin;

    private String nickName;

    private String openId;

    private String hxId;

    private String name;

    private String phone;

}

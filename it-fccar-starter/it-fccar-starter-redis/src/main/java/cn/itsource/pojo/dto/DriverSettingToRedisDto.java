package cn.itsource.pojo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * <p>
 * 司机配置
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
@Data
@Accessors(chain = true)
public class DriverSettingToRedisDto implements Serializable {

    private static final long serialVersionUID=1L;

    private Long id;

    private Boolean autoAccept;

    private Boolean orientation;

    private Boolean listenService;

    private Integer orderDistance;

    private Integer rangeDistance;

}

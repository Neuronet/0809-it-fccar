package cn.itsource.template;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import cn.itsource.pojo.dto.LoginInfoDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.Assert;

public class LoginTemplate {

    @Value("${sa-token.login-context-info-redis-key}")
    private String loginContextInfoRedisKey;

    private RedisTemplate redisTemplate;

    public LoginTemplate (RedisTemplate redisTemplate){
        this.redisTemplate = redisTemplate;
    }

    public LoginInfoDto getLoginInfo(Long loginId){
        String loginInfoKey = String.format(loginContextInfoRedisKey, loginId);
        Object loginInfoObj = redisTemplate.opsForValue().get(loginInfoKey);
        Assert.isTrue(ObjectUtil.isNotEmpty(loginInfoObj), "获取用户信息失败！");
        return JSONUtil.toBean(JSONUtil.toJsonStr(loginInfoObj), LoginInfoDto.class);
    }

}

package cn.itsource.template;

import cn.itsource.pojo.dto.DriverFromRedisGeoDto;
import cn.itsource.pojo.properties.GeoSettingProperties;
import org.springframework.data.geo.*;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.RedisTemplate;

import java.math.BigDecimal;
import java.util.List;

public class RedisGeoTemplate {

    private RedisTemplate redisTemplate;
    private GeoSettingProperties geoSettingProperties;

    public RedisGeoTemplate(RedisTemplate redisTemplate, GeoSettingProperties geoSettingProperties){
        this.redisTemplate = redisTemplate;
        this.geoSettingProperties = geoSettingProperties;
    }

    /**
     * 添加GEO坐标
     * @param key：GEO坐标的大Key
     * @param longitude：经度
     * @param latitude：纬度
     * @param member：GEO坐标的小Key
     */
    public void add(String key, String longitude, String latitude, String member){
        redisTemplate.opsForGeo().add(key,
                new Point(Double.valueOf(longitude), Double.valueOf(latitude)), member);
    }


    /**
     * 根据半径搜索司机
     * @param key
     * @param longitude
     * @param latitude
     */
    public List<DriverFromRedisGeoDto> searchGeo(String key, String longitude, String latitude){
        // 1.指定中心坐标的经纬度，以及搜索的半径多大
        Circle circle = new Circle(new Point(Double.valueOf(longitude), Double.valueOf(latitude)), new Distance(Double.valueOf(geoSettingProperties.getRadius()),Metrics.KILOMETERS));
        // 2.创建GEO参数对象
        RedisGeoCommands.GeoRadiusCommandArgs args = RedisGeoCommands.GeoRadiusCommandArgs.newGeoRadiusArgs();
        // 3.设置参数
        args.includeDistance()
                // 3.1.结果中包含坐标
                .includeCoordinates()
                // 3.2.按照中心点距离近到远排序
                .sortAscending()
                // 3.3.匹配前几个司机
                .limit(Long.valueOf(geoSettingProperties.getLimit()));
        // 4.调用GEO半径搜索的方法，把参数设置进去搜索司机
        GeoResults<RedisGeoCommands.GeoLocation<Object>> geoResults = redisTemplate.opsForGeo().radius(key, circle, args);

        // 5.拿到GEO结果列表
        List<GeoResult<RedisGeoCommands.GeoLocation<Object>>> geoResultList = geoResults.getContent();

        // map()：中间操作，对流中的每条数据做操作
        List<DriverFromRedisGeoDto> driverFromRedisGeoDtos = geoResultList.stream()
                .map(g -> new DriverFromRedisGeoDto(new BigDecimal(g.getDistance().getValue()), String.valueOf(g.getContent().getName())))
                .toList();
        return driverFromRedisGeoDtos;
    }

}

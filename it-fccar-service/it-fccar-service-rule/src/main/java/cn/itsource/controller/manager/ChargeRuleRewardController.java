package cn.itsource.controller.manager;

import cn.itsource.service.IChargeRuleRewardService;
import cn.itsource.pojo.domain.ChargeRuleReward;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import javax.validation.Valid;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.itsource.pojo.query.PageQueryWrapper;
import cn.itsource.result.R;
import cn.itsource.result.PageResult;

@Tag(name = "",description = "")
@RestController
@RequestMapping("/manager/chargeRuleReward")
public class ChargeRuleRewardController{

    @Autowired
    public IChargeRuleRewardService chargeRuleRewardService;

    @Operation( summary= "保存ChargeRuleReward",description = "基础对象保存接口")
    @Parameter(name = "chargeRuleReward",description = "保存的对象",required = true)
    @PostMapping
    public R save(@RequestBody @Valid ChargeRuleReward chargeRuleReward){
        return R.success(chargeRuleRewardService.save(chargeRuleReward));
    }

    @Operation( summary= "修改ChargeRuleReward",description = "基础对象修改接口")
    @Parameter(name = "chargeRuleReward",description = "修改的对象",required = true)
    @PutMapping
    public R update(@RequestBody  @Valid ChargeRuleReward chargeRuleReward){
        return R.success(chargeRuleRewardService.updateById(chargeRuleReward));
    }

    //删除对象
    @Operation( summary= "删除ChargeRuleReward",description = "基础对象删除接口，采用状态删除")
    @Parameter(name = "id",description = "删除的对象ID",required = true)
    @DeleteMapping(value="/{id}")
    public R delete(@PathVariable("id") Long id){
        return R.success(chargeRuleRewardService.removeById(id));
    }

    //获取对象
    @Operation( summary= "获取ChargeRuleReward",description = "基础对象获取接口")
    @Parameter(name = "id",description = "查询的对象ID",required = true)
    @GetMapping(value = "/{id}")
    public R get(@PathVariable("id")Long id){
        return R.success(chargeRuleRewardService.getById(id));
    }

    //获取列表:PageQueryWrapper作为通用的查询对象
    @Operation( summary= "查询ChargeRuleReward列表",description = "基础对象列表查询，不带分页")
    @Parameter(name = "query",description = "查询条件",required = true)
    @PostMapping(value = "/list")
    public R list(@RequestBody PageQueryWrapper<ChargeRuleReward> query){
        QueryWrapper<ChargeRuleReward> wrapper = new QueryWrapper<>();
        //实体类作为查询条件
        wrapper.setEntity(query.getQuery());
        return R.success(chargeRuleRewardService.list(wrapper));
    }

    //分页查询
    @Operation( summary= "查询ChargeRuleReward分页列表",description = "基础对象列表查询，带分页")
    @Parameter(name = "query",description = "查询条件，需要指定分页条件",required = true)
    @PostMapping(value = "/pagelist")
    public R page(@RequestBody PageQueryWrapper<ChargeRuleReward> query){
        //分页查询
        Page<ChargeRuleReward> page = new Page<ChargeRuleReward>(query.getPage(),query.getRows());
        QueryWrapper<ChargeRuleReward> wrapper = new QueryWrapper<>();
        //实体类作为查询条件
        wrapper.setEntity(query.getQuery());
        //分页查询
        page = chargeRuleRewardService.page(page,wrapper);
        //返回结果
        return R.success(new PageResult<ChargeRuleReward>(page.getTotal(),page.getRecords()));
    }

}

package cn.itsource.controller.manager;

import cn.itsource.service.IProfitsharingRuleOtherFeeService;
import cn.itsource.pojo.domain.ProfitsharingRuleOtherFee;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import javax.validation.Valid;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.itsource.pojo.query.PageQueryWrapper;
import cn.itsource.result.R;
import cn.itsource.result.PageResult;

@Tag(name = "其他费用：税，渠道费",description = "其他费用：税，渠道费")
@RestController
@RequestMapping("/manager/profitsharingRuleOtherFee")
public class ProfitsharingRuleOtherFeeController{

    @Autowired
    public IProfitsharingRuleOtherFeeService profitsharingRuleOtherFeeService;

    @Operation( summary= "保存ProfitsharingRuleOtherFee",description = "基础对象保存接口")
    @Parameter(name = "profitsharingRuleOtherFee",description = "保存的对象",required = true)
    @PostMapping
    public R save(@RequestBody @Valid ProfitsharingRuleOtherFee profitsharingRuleOtherFee){
        return R.success(profitsharingRuleOtherFeeService.save(profitsharingRuleOtherFee));
    }

    @Operation( summary= "修改ProfitsharingRuleOtherFee",description = "基础对象修改接口")
    @Parameter(name = "profitsharingRuleOtherFee",description = "修改的对象",required = true)
    @PutMapping
    public R update(@RequestBody  @Valid ProfitsharingRuleOtherFee profitsharingRuleOtherFee){
        return R.success(profitsharingRuleOtherFeeService.updateById(profitsharingRuleOtherFee));
    }

    //删除对象
    @Operation( summary= "删除ProfitsharingRuleOtherFee",description = "基础对象删除接口，采用状态删除")
    @Parameter(name = "id",description = "删除的对象ID",required = true)
    @DeleteMapping(value="/{id}")
    public R delete(@PathVariable("id") Long id){
        return R.success(profitsharingRuleOtherFeeService.removeById(id));
    }

    //获取对象
    @Operation( summary= "获取ProfitsharingRuleOtherFee",description = "基础对象获取接口")
    @Parameter(name = "id",description = "查询的对象ID",required = true)
    @GetMapping(value = "/{id}")
    public R get(@PathVariable("id")Long id){
        return R.success(profitsharingRuleOtherFeeService.getById(id));
    }

    //获取列表:PageQueryWrapper作为通用的查询对象
    @Operation( summary= "查询ProfitsharingRuleOtherFee列表",description = "基础对象列表查询，不带分页")
    @Parameter(name = "query",description = "查询条件",required = true)
    @PostMapping(value = "/list")
    public R list(@RequestBody PageQueryWrapper<ProfitsharingRuleOtherFee> query){
        QueryWrapper<ProfitsharingRuleOtherFee> wrapper = new QueryWrapper<>();
        //实体类作为查询条件
        wrapper.setEntity(query.getQuery());
        return R.success(profitsharingRuleOtherFeeService.list(wrapper));
    }

    //分页查询
    @Operation( summary= "查询ProfitsharingRuleOtherFee分页列表",description = "基础对象列表查询，带分页")
    @Parameter(name = "query",description = "查询条件，需要指定分页条件",required = true)
    @PostMapping(value = "/pagelist")
    public R page(@RequestBody PageQueryWrapper<ProfitsharingRuleOtherFee> query){
        //分页查询
        Page<ProfitsharingRuleOtherFee> page = new Page<ProfitsharingRuleOtherFee>(query.getPage(),query.getRows());
        QueryWrapper<ProfitsharingRuleOtherFee> wrapper = new QueryWrapper<>();
        //实体类作为查询条件
        wrapper.setEntity(query.getQuery());
        //分页查询
        page = profitsharingRuleOtherFeeService.page(page,wrapper);
        //返回结果
        return R.success(new PageResult<ProfitsharingRuleOtherFee>(page.getTotal(),page.getRecords()));
    }

}

package cn.itsource.mapper;

import cn.itsource.pojo.domain.ProfitsharingRuleOtherFee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 其他费用：税，渠道费 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface ProfitsharingRuleOtherFeeMapper extends BaseMapper<ProfitsharingRuleOtherFee> {

}

package cn.itsource.mapper;

import cn.itsource.pojo.domain.ChargeRuleReturn;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 返程费计价规则 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface ChargeRuleReturnMapper extends BaseMapper<ChargeRuleReturn> {

}

package cn.itsource.mapper;

import cn.itsource.pojo.domain.ChargeRuleReward;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface ChargeRuleRewardMapper extends BaseMapper<ChargeRuleReward> {

}

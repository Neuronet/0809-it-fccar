package cn.itsource.mapper;

import cn.itsource.pojo.domain.ChargeRuleStart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 计价规则-起步价 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface ChargeRuleStartMapper extends BaseMapper<ChargeRuleStart> {

}

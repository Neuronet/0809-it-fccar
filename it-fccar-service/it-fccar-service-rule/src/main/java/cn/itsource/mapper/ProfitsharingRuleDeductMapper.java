package cn.itsource.mapper;

import cn.itsource.pojo.domain.ProfitsharingRuleDeduct;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 司机罚款 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface ProfitsharingRuleDeductMapper extends BaseMapper<ProfitsharingRuleDeduct> {

}

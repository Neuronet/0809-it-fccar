package cn.itsource.mapper;

import cn.itsource.pojo.domain.ChargeRuleWait;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 等待费用 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface ChargeRuleWaitMapper extends BaseMapper<ChargeRuleWait> {

}

package cn.itsource.mapper;

import cn.itsource.pojo.domain.ProfitsharingRuleAttach;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 平台抽成附加规则 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface ProfitsharingRuleAttachMapper extends BaseMapper<ProfitsharingRuleAttach> {

}

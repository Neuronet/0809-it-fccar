package cn.itsource.pojo.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * <p>
 * 平台抽成附加规则
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_profitsharing_rule_attach")
@Schema(name = "ProfitsharingRuleAttach对象", description = "平台抽成附加规则")
public class ProfitsharingRuleAttach implements Serializable {

    private static final long serialVersionUID=1L;

    @Schema(name = "id", description = "ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @Schema(name = "driverRatioAdd", description = "追加抽成比例")
    @TableField("driver_ratio_add")
    private BigDecimal driverRatioAdd;

    @Schema(name = "exceedAmountFrom", description = "订单金额从多少")
    @TableField("exceed_amount_from")
    private BigDecimal exceedAmountFrom;

    @Schema(name = "exceedAmountTo", description = "订单金额到多少")
    @TableField("exceed_amount_to")
    private BigDecimal exceedAmountTo;

    @Schema(name = "createTime", description = "创建时间")
    @TableField("create_time")
    private Date createTime;

    @Schema(name = "updateTime", description = "修改时间")
    @TableField("update_time")
    private Date updateTime;

    @Schema(name = "deleted", description = "逻辑删除")
    @TableField("deleted")
    @TableLogic
    private Boolean deleted;

    @Schema(name = "version", description = "乐观锁")
    @TableField("version")
    @Version
    private Integer version;

}

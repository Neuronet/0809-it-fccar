package cn.itsource.remote;

import cn.itsource.pojo.dto.MileagePricingDto;
import cn.itsource.feign.RuleApi;
import cn.itsource.result.R;
import cn.itsource.service.IChargeRuleStartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RuleRemote implements RuleApi {

    @Autowired
    private IChargeRuleStartService chargeRuleStartService;

    /**
     * 计算里程价格
     * @param mileagePricingDto
     * @return
     */
    @Override
    public R mileagePricing(MileagePricingDto mileagePricingDto) {
        return R.success(chargeRuleStartService.mileagePricing(mileagePricingDto));
    }
}

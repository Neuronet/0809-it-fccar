package cn.itsource.service;

import cn.itsource.pojo.domain.ChargeRuleReward;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface IChargeRuleRewardService extends IService<ChargeRuleReward> {

}

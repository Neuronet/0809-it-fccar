package cn.itsource.service;

import cn.itsource.pojo.domain.ProfitsharingRuleOtherFee;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 其他费用：税，渠道费 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface IProfitsharingRuleOtherFeeService extends IService<ProfitsharingRuleOtherFee> {

}

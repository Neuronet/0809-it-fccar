package cn.itsource.service.impl;

import cn.itsource.pojo.domain.ProfitsharingRuleBase;
import cn.itsource.mapper.ProfitsharingRuleBaseMapper;
import cn.itsource.service.IProfitsharingRuleBaseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 平台抽成基础比例 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
@Service
public class ProfitsharingRuleBaseServiceImpl extends ServiceImpl<ProfitsharingRuleBaseMapper, ProfitsharingRuleBase> implements IProfitsharingRuleBaseService {

}

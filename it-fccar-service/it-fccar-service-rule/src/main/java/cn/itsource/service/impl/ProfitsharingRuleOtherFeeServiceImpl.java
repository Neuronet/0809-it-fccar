package cn.itsource.service.impl;

import cn.itsource.pojo.domain.ProfitsharingRuleOtherFee;
import cn.itsource.mapper.ProfitsharingRuleOtherFeeMapper;
import cn.itsource.service.IProfitsharingRuleOtherFeeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 其他费用：税，渠道费 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
@Service
public class ProfitsharingRuleOtherFeeServiceImpl extends ServiceImpl<ProfitsharingRuleOtherFeeMapper, ProfitsharingRuleOtherFee> implements IProfitsharingRuleOtherFeeService {

}

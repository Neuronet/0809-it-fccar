package cn.itsource.service.impl;

import cn.itsource.pojo.domain.ChargeRuleReward;
import cn.itsource.mapper.ChargeRuleRewardMapper;
import cn.itsource.service.IChargeRuleRewardService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
@Service
public class ChargeRuleRewardServiceImpl extends ServiceImpl<ChargeRuleRewardMapper, ChargeRuleReward> implements IChargeRuleRewardService {

}

package cn.itsource.service.impl;

import cn.itsource.pojo.domain.ChargeRuleWait;
import cn.itsource.mapper.ChargeRuleWaitMapper;
import cn.itsource.service.IChargeRuleWaitService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 等待费用 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
@Service
public class ChargeRuleWaitServiceImpl extends ServiceImpl<ChargeRuleWaitMapper, ChargeRuleWait> implements IChargeRuleWaitService {

}

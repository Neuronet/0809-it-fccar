package cn.itsource.service.impl;

import cn.itsource.pojo.domain.ProfitsharingRuleDeduct;
import cn.itsource.mapper.ProfitsharingRuleDeductMapper;
import cn.itsource.service.IProfitsharingRuleDeductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 司机罚款 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
@Service
public class ProfitsharingRuleDeductServiceImpl extends ServiceImpl<ProfitsharingRuleDeductMapper, ProfitsharingRuleDeduct> implements IProfitsharingRuleDeductService {

}

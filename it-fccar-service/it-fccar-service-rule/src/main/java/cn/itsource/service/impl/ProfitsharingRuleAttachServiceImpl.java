package cn.itsource.service.impl;

import cn.itsource.pojo.domain.ProfitsharingRuleAttach;
import cn.itsource.mapper.ProfitsharingRuleAttachMapper;
import cn.itsource.service.IProfitsharingRuleAttachService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 平台抽成附加规则 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
@Service
public class ProfitsharingRuleAttachServiceImpl extends ServiceImpl<ProfitsharingRuleAttachMapper, ProfitsharingRuleAttach> implements IProfitsharingRuleAttachService {

}

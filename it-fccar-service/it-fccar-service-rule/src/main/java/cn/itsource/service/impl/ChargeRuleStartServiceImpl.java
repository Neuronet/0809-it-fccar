package cn.itsource.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.itsource.constants.Constants;
import cn.itsource.constants.GlobalExceptionCode;
import cn.itsource.mapper.ChargeRuleReturnMapper;
import cn.itsource.mapper.ChargeRuleWaitMapper;
import cn.itsource.pojo.domain.ChargeRuleReturn;
import cn.itsource.pojo.domain.ChargeRuleStart;
import cn.itsource.mapper.ChargeRuleStartMapper;
import cn.itsource.pojo.domain.ChargeRuleWait;
import cn.itsource.pojo.dto.MileagePricingDto;
import cn.itsource.pojo.dto.MileagePricingResultDto;
import cn.itsource.service.IChargeRuleStartService;
import cn.itsource.utils.AssertUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 计价规则-起步价 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
@Service
public class ChargeRuleStartServiceImpl extends ServiceImpl<ChargeRuleStartMapper, ChargeRuleStart> implements IChargeRuleStartService {

    @Autowired
    private ChargeRuleReturnMapper chargeRuleReturnMapper;

    @Autowired
    private ChargeRuleWaitMapper chargeRuleWaitMapper;

    /**
     * 里程计价
     * @param mileagePricingDto
     * @return
     */
    @Override
    public MileagePricingResultDto mileagePricing(MileagePricingDto mileagePricingDto) {
        Date pricingTime = mileagePricingDto.getPricingTime();
        BigDecimal mileage = mileagePricingDto.getMileage();
        // 1.参数校验-JSR303
        // 2.业务校验
        // 2.1.里程规则
        int hour = DateUtil.hour(pricingTime, true);
        // 开始时间小于等于当前时间  结束时间大于等于当前时间
        ChargeRuleStart chargeRuleStart = super.getOne(new LambdaQueryWrapper<ChargeRuleStart>()
                .ge(ChargeRuleStart::getHourEnd, hour)
                .le(ChargeRuleStart::getHourStart, hour));
        AssertUtil.isNotNull(chargeRuleStart, GlobalExceptionCode.SERVICE_ERROR);


        // 基础里程
        BigDecimal baseMileage = chargeRuleStart.getBaseMileage();
        // 基础里程价格
        BigDecimal amount = chargeRuleStart.getAmount();
        // 超出基础里程每公里价格
        BigDecimal exceedEveryKmAmount = chargeRuleStart.getExceedEveryKmAmount();


        // 2.2.返程规则
        ChargeRuleReturn chargeRuleReturn = chargeRuleReturnMapper.selectOne(new LambdaQueryWrapper<ChargeRuleReturn>().eq(ChargeRuleReturn::getEnable, Constants.Rule.ENABLE));
        AssertUtil.isNotNull(chargeRuleReturn, GlobalExceptionCode.SERVICE_ERROR);

        // 返程免费公里数
        BigDecimal freeBaseReturnMileage = chargeRuleReturn.getFreeBaseReturnMileage();
        // 返程超出基础里程每公里费用
        BigDecimal exceedReturnEveryKmAmount = chargeRuleReturn.getExceedEveryKmAmount();

        // 2.3.等时规则
        ChargeRuleWait chargeRuleWait = chargeRuleWaitMapper.selectOne(new LambdaQueryWrapper<ChargeRuleWait>().eq(ChargeRuleWait::getEnable, Constants.Rule.ENABLE));
        AssertUtil.isNotNull(chargeRuleWait, GlobalExceptionCode.SERVICE_ERROR);

        // 3.业务实现
        MileagePricingResultDto mileagePricingResultDto = new MileagePricingResultDto();
        BigDecimal mileageAmount = BigDecimal.ZERO;
        mileageAmount = mileageAmount.add(amount);

        // 3.1.里程价格
        if(mileage.compareTo(baseMileage) == 1){
            BigDecimal freeStart = mileage.subtract(baseMileage);
            BigDecimal exceedBaseMileageAmount = freeStart.multiply(exceedEveryKmAmount);
            mileagePricingResultDto.setExceedBaseMileageAmount(exceedBaseMileageAmount);

            mileageAmount = mileageAmount.add(exceedBaseMileageAmount);
        }

        // 3.2.返程价格
        if(mileage.compareTo(freeBaseReturnMileage) == 1){
            BigDecimal freeReturn = mileage.subtract(freeBaseReturnMileage);
            BigDecimal returnAmount = freeReturn.multiply(exceedReturnEveryKmAmount);
            mileagePricingResultDto.setReturnAmont(returnAmount);

            mileageAmount = mileageAmount.add(returnAmount);
        }

        // 3.3.计算等时费用
        Integer waitingMinute = mileagePricingDto.getWaitingMinute();
        Integer freeBaseWaitingMinute = chargeRuleWait.getFreeBaseWaitingMinute();
        BigDecimal exceedEveryMinuteAmount = chargeRuleWait.getExceedEveryMinuteAmount();
        if(waitingMinute != null && waitingMinute > 0 && waitingMinute > freeBaseWaitingMinute){
            Integer exceedEveryMinute = waitingMinute - freeBaseWaitingMinute;
            BigDecimal waitAmout = exceedEveryMinuteAmount.multiply(new BigDecimal(exceedEveryMinute));
            mileagePricingResultDto.setWaitingAmount(waitAmout);
            mileageAmount = mileageAmount.add(waitAmout);
        }


        mileagePricingResultDto.setRealOrderAmount(mileageAmount);
        mileagePricingResultDto.setBaseMileage(baseMileage);
        mileagePricingResultDto.setBaseMileageAmount(amount);
        mileagePricingResultDto.setMileageAmount(mileageAmount);
        mileagePricingResultDto.setFreeBaseWaitingMinute(freeBaseWaitingMinute);
        mileagePricingResultDto.setFreeBaseReturnMileage(freeBaseReturnMileage);
        mileagePricingResultDto.setExceedBaseReturnEveryKmAmount(exceedReturnEveryKmAmount);
        mileagePricingResultDto.setExceedEveryMinuteAmount(exceedEveryMinuteAmount);

        return mileagePricingResultDto;
    }
}

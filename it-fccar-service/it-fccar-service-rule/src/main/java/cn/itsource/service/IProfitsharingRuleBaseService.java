package cn.itsource.service;

import cn.itsource.pojo.domain.ProfitsharingRuleBase;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 平台抽成基础比例 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface IProfitsharingRuleBaseService extends IService<ProfitsharingRuleBase> {

}

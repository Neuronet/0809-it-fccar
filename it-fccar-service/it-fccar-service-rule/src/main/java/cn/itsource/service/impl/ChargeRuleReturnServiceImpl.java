package cn.itsource.service.impl;

import cn.itsource.pojo.domain.ChargeRuleReturn;
import cn.itsource.mapper.ChargeRuleReturnMapper;
import cn.itsource.service.IChargeRuleReturnService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 返程费计价规则 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
@Service
public class ChargeRuleReturnServiceImpl extends ServiceImpl<ChargeRuleReturnMapper, ChargeRuleReturn> implements IChargeRuleReturnService {

}

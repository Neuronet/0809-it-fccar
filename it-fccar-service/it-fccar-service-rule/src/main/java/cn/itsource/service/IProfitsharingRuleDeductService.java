package cn.itsource.service;

import cn.itsource.pojo.domain.ProfitsharingRuleDeduct;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 司机罚款 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface IProfitsharingRuleDeductService extends IService<ProfitsharingRuleDeduct> {

}

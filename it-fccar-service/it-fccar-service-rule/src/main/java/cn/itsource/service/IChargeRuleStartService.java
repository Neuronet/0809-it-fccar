package cn.itsource.service;

import cn.itsource.pojo.dto.MileagePricingDto;
import cn.itsource.pojo.domain.ChargeRuleStart;
import cn.itsource.pojo.dto.MileagePricingResultDto;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 计价规则-起步价 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface IChargeRuleStartService extends IService<ChargeRuleStart> {

    MileagePricingResultDto mileagePricing(MileagePricingDto mileagePricingDto);
}

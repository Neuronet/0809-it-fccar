package cn.itsource.service;

import cn.itsource.pojo.domain.ChargeRuleReturn;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 返程费计价规则 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface IChargeRuleReturnService extends IService<ChargeRuleReturn> {

}

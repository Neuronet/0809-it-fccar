package cn.itsource.service;

import cn.itsource.pojo.domain.ChargeRuleWait;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 等待费用 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface IChargeRuleWaitService extends IService<ChargeRuleWait> {

}

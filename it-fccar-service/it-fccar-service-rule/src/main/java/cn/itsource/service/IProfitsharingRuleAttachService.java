package cn.itsource.service;

import cn.itsource.pojo.domain.ProfitsharingRuleAttach;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 平台抽成附加规则 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface IProfitsharingRuleAttachService extends IService<ProfitsharingRuleAttach> {

}

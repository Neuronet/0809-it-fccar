package cn.itsource.pojo.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class DriverPullOrderDto {

   // 订单距离
   private BigDecimal distance;

   // 订单预估里程
   private BigDecimal expectsMileage;

   // 订单总金额
   private BigDecimal realOrderAmount;

   // 好处费/加价
   private BigDecimal favourAmount = BigDecimal.ZERO;

   // 开始地点
   private String startPlace;

   // 结束地点
   private String endPlace;

   // 订单编号
   private String orderNo;

   // 预计分钟数
   private Integer expectMinutes;

}

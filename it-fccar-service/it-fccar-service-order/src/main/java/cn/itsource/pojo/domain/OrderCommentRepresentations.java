package cn.itsource.pojo.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * <p>
 * 差评申述
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_order_comment_representations")
@Schema(name = "OrderCommentRepresentations对象", description = "差评申述")
public class OrderCommentRepresentations implements Serializable {

    private static final long serialVersionUID=1L;

    @Schema(name = "id", description = "ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @Schema(name = "driverId", description = "司机ID")
    @TableField("driver_id")
    private Long driverId;

    @Schema(name = "driverName", description = "司机名字")
    @TableField("driver_name")
    private String driverName;

    @Schema(name = "customerId", description = "乘客ID")
    @TableField("customer_id")
    private Long customerId;

    @Schema(name = "customerName", description = "乘客名字")
    @TableField("customer_name")
    private String customerName;

    @Schema(name = "commentContent", description = "评级内容")
    @TableField("comment_content")
    private String commentContent;

    @Schema(name = "orderCommontId", description = "评价ID")
    @TableField("order_commont_id")
    private Long orderCommontId;

    @Schema(name = "status", description = "状态:0待审核，1通过，2驳回")
    @TableField("status")
    private Integer status;

    @Schema(name = "createTime", description = "创建时间")
    @TableField("create_time")
    private Date createTime;

    @Schema(name = "updateTime", description = "最后修改时间")
    @TableField("update_time")
    private Date updateTime;

    @Schema(name = "auditDesc", description = "审核备注")
    @TableField("audit_desc")
    private String auditDesc;

    @Schema(name = "driverReason", description = "申述理由")
    @TableField("driver_reason")
    private String driverReason;

    @Schema(name = "auditLoginId", description = "")
    @TableField("audit_login_id")
    private Long auditLoginId;

    @Schema(name = "auditLoginUsername", description = "")
    @TableField("audit_login_username")
    private String auditLoginUsername;

}

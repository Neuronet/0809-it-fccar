package cn.itsource.pojo.vo;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class OrderLocationInfoVo {

    private Integer status;

    private String startPlaceLatitude;

    private String startPlaceLongitude;

    private String endPlaceLatitude;

    private String endPlaceLongitude;

}

package cn.itsource.pojo.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class CostomerCreateOrderDto {

    @Schema(name = "startPlace", description = "起始地点")
    @TableField("start_place")
    private String startPlace;

    @Schema(name = "expectsMileage", description = "预估里程")
    @TableField("expects_mileage")
    private BigDecimal expectsMileage;

    @Schema(name = "endPlace", description = "结束地点")
    @TableField("end_place")
    private String endPlace;

    @Schema(name = "carPlate", description = "车牌号")
    @TableField("car_plate")
    private String carPlate;

    @Schema(name = "carType", description = "车型")
    @TableField("car_type")
    private String carType;

    @Schema(name = "expectMinutes", description = "预计分钟数")
    @TableField("expect_minutes")
    private Integer expectMinutes;

    @Schema(name = "startPlaceLongitude", description = "开始位置经度")
    @TableField("start_place_longitude")
    private String startPlaceLongitude;

    @Schema(name = "startPlaceLatitude", description = "开始位置纬度")
    @TableField("start_place_latitude")
    private String startPlaceLatitude;

    @Schema(name = "endPlaceLongitude", description = "结束位置经度")
    @TableField("end_place_longitude")
    private String endPlaceLongitude;

    @Schema(name = "endPlaceLatiude", description = "结束位置纬度")
    @TableField("end_place_latiude")
    private String endPlaceLatiude;

}

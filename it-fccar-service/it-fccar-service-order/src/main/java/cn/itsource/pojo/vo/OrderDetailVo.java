package cn.itsource.pojo.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class OrderDetailVo {

    // 司机信息
    private String driverName;
    private String driverPhone;
    private String driverPhoto;

    // 开始结束位置
    private String startPlace;
    private String endPlace;

    // 订单基础信息
    private String orderNo;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
    private Date createTime;
    private BigDecimal favourAmount = BigDecimal.ZERO;

    // 车辆信息
    private String carPlate;
    private String carType;

    // 基础收费
    // 实际里程
    private BigDecimal realMileage;
    // 预估里程
    private BigDecimal expectsMileage;

    private BigDecimal baseMileageAmount;
    private BigDecimal baseMileage;
    private BigDecimal exceedBaseMileageAmount;
    private BigDecimal mileageAmount;

    // 等待费用
    private Integer waitingMinute;
    private Integer freeBaseWaitingMinute;
    private BigDecimal exceedEveryMinuteAmount;
    private BigDecimal waitingAmount = BigDecimal.ZERO;

    // 返程费用
    private BigDecimal freeBaseReturnMileage;
    private BigDecimal exceedBaseReturnEveryKmAmount;
    private BigDecimal returnAmont;

    // 额外收费
    private BigDecimal parkingAmount = BigDecimal.ZERO;
    private BigDecimal tollAmount = BigDecimal.ZERO;
    private BigDecimal otherAmount = BigDecimal.ZERO;

    // 见面金额
    private BigDecimal voucherAmount = BigDecimal.ZERO;

    // 实付金额
    private BigDecimal realPayAmount = BigDecimal.ZERO;

    // 状态
    private Integer status;

    // 汇总合计费用
    public BigDecimal getTotal(){
        return getMileageAmount().add(getWaitingAmount()).add(getReturnAmont()).add(getParkingAmount())
                .add(getTollAmount()).add(getOtherAmount());
    }

}

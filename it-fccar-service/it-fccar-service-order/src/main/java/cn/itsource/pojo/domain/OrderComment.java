package cn.itsource.pojo.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * <p>
 * 订单评价表
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_order_comment")
@Schema(name = "OrderComment对象", description = "订单评价表")
public class OrderComment implements Serializable {

    private static final long serialVersionUID=1L;

    @Schema(name = "id", description = "主键")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @Schema(name = "orderNo", description = "订单编号")
    @TableField("order_no")
    private String orderNo;

    @Schema(name = "driverId", description = "司机ID")
    @TableField("driver_id")
    private Long driverId;

    @Schema(name = "customerId", description = "顾客ID")
    @TableField("customer_id")
    private Long customerId;

    @Schema(name = "rate", description = "评分，1星~5星")
    @TableField("rate")
    private Integer rate;

    @Schema(name = "remark", description = "评价备注")
    @TableField("remark")
    private String remark;

    @Schema(name = "createTime", description = "创建时间")
    @TableField("create_time")
    private Date createTime;

    @Schema(name = "status", description = "状态：0默认 ，1司机申述，2申述通过，3申述驳回")
    @TableField("status")
    private Integer status;

    @Schema(name = "customerPhoto", description = "乘客头像")
    @TableField("customer_photo")
    private String customerPhoto;

    @Schema(name = "customerName", description = "乘客名字")
    @TableField("customer_name")
    private String customerName;

    @Schema(name = "driverName", description = "司机名字")
    @TableField("driver_name")
    private String driverName;

    @Schema(name = "driverPhoto", description = "司机头像")
    @TableField("driver_photo")
    private String driverPhoto;

    @Schema(name = "driverReason", description = "申述理由")
    @TableField("driver_reason")
    private String driverReason;

    @Schema(name = "auditLoginId", description = "审核人")
    @TableField("audit_login_id")
    private Long auditLoginId;

    @Schema(name = "auditLoginUsername", description = "审核人")
    @TableField("audit_login_username")
    private String auditLoginUsername;

    @Schema(name = "auditDesc", description = "审核备注")
    @TableField("audit_desc")
    private String auditDesc;

    @Schema(name = "auditTime", description = "审核时间")
    @TableField("audit_time")
    private Date auditTime;

    @Schema(name = "appealTime", description = "申述时间")
    @TableField("appeal_time")
    private Date appealTime;

}

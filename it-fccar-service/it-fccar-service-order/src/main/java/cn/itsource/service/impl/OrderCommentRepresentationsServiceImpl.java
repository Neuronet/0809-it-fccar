package cn.itsource.service.impl;

import cn.itsource.pojo.domain.OrderCommentRepresentations;
import cn.itsource.mapper.OrderCommentRepresentationsMapper;
import cn.itsource.service.IOrderCommentRepresentationsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 差评申述 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
@Service
public class OrderCommentRepresentationsServiceImpl extends ServiceImpl<OrderCommentRepresentationsMapper, OrderCommentRepresentations> implements IOrderCommentRepresentationsService {

}

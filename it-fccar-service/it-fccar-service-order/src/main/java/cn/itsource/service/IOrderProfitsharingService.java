package cn.itsource.service;

import cn.itsource.pojo.domain.OrderProfitsharing;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 平台司机分账表 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface IOrderProfitsharingService extends IService<OrderProfitsharing> {

}

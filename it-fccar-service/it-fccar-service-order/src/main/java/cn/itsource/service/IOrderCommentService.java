package cn.itsource.service;

import cn.itsource.pojo.domain.OrderComment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单评价表 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface IOrderCommentService extends IService<OrderComment> {

}

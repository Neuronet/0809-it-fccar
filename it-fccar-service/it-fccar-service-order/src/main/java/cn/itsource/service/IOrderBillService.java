package cn.itsource.service;

import cn.itsource.pojo.domain.OrderBill;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单账单表 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface IOrderBillService extends IService<OrderBill> {

    OrderBill selectByOrderNo(String orderNo);
}

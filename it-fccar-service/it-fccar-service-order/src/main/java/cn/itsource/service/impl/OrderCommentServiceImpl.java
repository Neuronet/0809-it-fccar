package cn.itsource.service.impl;

import cn.itsource.pojo.domain.OrderComment;
import cn.itsource.mapper.OrderCommentMapper;
import cn.itsource.service.IOrderCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单评价表 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
@Service
public class OrderCommentServiceImpl extends ServiceImpl<OrderCommentMapper, OrderComment> implements IOrderCommentService {

}

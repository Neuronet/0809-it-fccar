package cn.itsource.service.impl;

import cn.itsource.constants.GlobalExceptionCode;
import cn.itsource.pojo.domain.OrderBill;
import cn.itsource.mapper.OrderBillMapper;
import cn.itsource.service.IOrderBillService;
import cn.itsource.utils.AssertUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单账单表 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
@Service
public class OrderBillServiceImpl extends ServiceImpl<OrderBillMapper, OrderBill> implements IOrderBillService {

    /**
     * 根据订单号查询账单
     * @param orderNo
     * @return
     */
    @Override
    public OrderBill selectByOrderNo(String orderNo) {
        OrderBill orderBill = super.getOne(new LambdaQueryWrapper<OrderBill>().eq(OrderBill::getOrderNo, orderNo));
        AssertUtil.isNotNull(orderBill, GlobalExceptionCode.SERVICE_ERROR);
        return orderBill;
    }
}

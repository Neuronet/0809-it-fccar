package cn.itsource.service;

import cn.itsource.pojo.domain.Order;
import cn.itsource.pojo.dto.CostomerCreateOrderDto;
import cn.itsource.pojo.dto.DriverPullOrderDto;
import cn.itsource.pojo.vo.OrderDetailVo;
import cn.itsource.pojo.vo.OrderLocationInfoVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface IOrderService extends IService<Order> {

    String create(CostomerCreateOrderDto costomerCreateOrderDto);

    void cancelHandle(String orderNo);

    void countEndHandle(String orderNo);

    Order getCustomerInPorcessOrder();

    Order selectOrderNo(String orderNo);

    List<DriverPullOrderDto> driverPullOrder();

    Order orderGrab(String orderNo);

    Order arrive(String orderNo);

    Order getOrderInProgress();

    OrderLocationInfoVo location(String orderNo);

    Order startDriving(String orderNo);

    OrderDetailVo orderDetail(String orderNo);

    String endDrivingHandle(String orderNo);
}

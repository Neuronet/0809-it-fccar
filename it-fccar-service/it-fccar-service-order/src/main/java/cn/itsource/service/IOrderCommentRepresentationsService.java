package cn.itsource.service;

import cn.itsource.pojo.domain.OrderCommentRepresentations;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 差评申述 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface IOrderCommentRepresentationsService extends IService<OrderCommentRepresentations> {

}

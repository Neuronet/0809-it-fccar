package cn.itsource.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import cn.itsource.constants.Constants;
import cn.itsource.constants.GlobalExceptionCode;
import cn.itsource.dto.CreateCustomerFineDto;
import cn.itsource.feign.AppDriverApi;
import cn.itsource.feign.CustomerFineApi;
import cn.itsource.feign.RuleApi;
import cn.itsource.mapper.OrderBillMapper;
import cn.itsource.mapper.OrderMapper;
import cn.itsource.pojo.domain.DriverPoint;
import cn.itsource.pojo.domain.Order;
import cn.itsource.pojo.domain.OrderBill;
import cn.itsource.pojo.domain.Point;
import cn.itsource.pojo.dto.*;
import cn.itsource.pojo.vo.OrderDetailVo;
import cn.itsource.pojo.vo.OrderLocationInfoVo;
import cn.itsource.result.R;
import cn.itsource.service.IOrderBillService;
import cn.itsource.service.IOrderService;
import cn.itsource.template.LoginTemplate;
import cn.itsource.template.RedisGeoTemplate;
import cn.itsource.template.TxMapTemplate;
import cn.itsource.utils.AssertUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.DefaultMQPullConsumer;
import org.apache.rocketmq.client.consumer.PullResult;
import org.apache.rocketmq.client.consumer.PullStatus;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.message.MessageQueue;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
@Slf4j
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {

    @Autowired
    private RuleApi ruleApi;

    @Autowired
    private LoginTemplate loginTemplate;

    @Autowired
    private OrderBillMapper orderBillMapper;

    @Autowired
    private RedisGeoTemplate redisGeoTemplate;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Autowired
    private CustomerFineApi customerFineApi;

    @Autowired
    private DefaultMQPullConsumer pullConsumer;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private IOrderBillService orderBillService;

    @Autowired
    private AppDriverApi appDriverApi;

    @Autowired
    private TxMapTemplate txMapTemplate;



    /**
     * 乘客下单
     * @param costomerCreateOrderDto
     */
    @Override
    @Transactional
    public String create(CostomerCreateOrderDto costomerCreateOrderDto) {
        // 1.参数校验-JSR303

        // 2.业务校验
        // 2.1.是否已经有进行中的订单
        long loginId = StpUtil.getLoginIdAsLong();
        Order progressOrder = super.getOne(new LambdaQueryWrapper<Order>()
                .eq(Order::getCustomerId, loginId)
                .in(Order::getStatus, Constants.Order.CUSTOMER_ORDER_IN_PROGRESS));
        AssertUtil.isNull(progressOrder, GlobalExceptionCode.SERVICE_ERROR);
        // 2.2.上一次订单没付钱-不做

        // 2.3.乘客是否被拉黑-不做

        // 3.业务实现
        // 3.1.计算价格
        Date date = new Date();
        MileagePricingDto mileagePricingDto = new MileagePricingDto();
        mileagePricingDto.setPricingTime(date);
        BigDecimal expectsMileage = costomerCreateOrderDto.getExpectsMileage();
        mileagePricingDto.setMileage(expectsMileage);
        R<MileagePricingResultDto> mileagePricingDtoR = ruleApi.mileagePricing(mileagePricingDto);
        AssertUtil.isTrue(mileagePricingDtoR.isSuccess(), GlobalExceptionCode.SERVICE_ERROR);
        MileagePricingResultDto mileagePricingResultDto = mileagePricingDtoR.getData();

        // 3.2.创建订单
        Order order = createOrder(mileagePricingResultDto, costomerCreateOrderDto);
        // 3.3.创建订单账单
        OrderBill orderBill = createOrderBill(order, mileagePricingResultDto);

        // 3.4.GEO查找司机
        List<DriverFromRedisGeoDto> driverFromRedisGeoDtos = redisGeoTemplate.searchGeo(Constants.Redis.DRIVER_LOCATION_KEY,
                costomerCreateOrderDto.getStartPlaceLongitude(), costomerCreateOrderDto.getStartPlaceLatitude());

        Boolean isSuccess = false;

        String orderNo = order.getOrderNo();

        // 3.5.根据司机配置进行过滤
        for (DriverFromRedisGeoDto driverFromRedisGeoDto : driverFromRedisGeoDtos) {
            // 1.从Redis中获取司机配置
            String driverId = driverFromRedisGeoDto.getMember();
            BigDecimal distance = driverFromRedisGeoDto.getDistance();
            String onlineKey = String.format(Constants.Redis.DRIVER_ONLINE_KEY, driverId);
            Object driverSettingObj = redisTemplate.opsForValue().get(onlineKey);
            // 2.如果获取不到此司机配置，说明此司机已下线，那么就跳过此司机
            if (ObjectUtil.isEmpty(driverSettingObj)){
                continue;
            }

            // 3.获取到司机配置
            DriverSettingToRedisDto driverSettingToRedisDto = JSONUtil.toBean(JSONUtil.toJsonStr(driverSettingObj), DriverSettingToRedisDto.class);
            Integer orderDistance = driverSettingToRedisDto.getOrderDistance();
            Integer rangeDistance = driverSettingToRedisDto.getRangeDistance();
            // 4.判断司机当前距离中心点的距离 > 司机配置的接单距离那么就不允许接单
            if(distance.compareTo(new BigDecimal(rangeDistance)) == 1){
                continue;
            }
            // 判断订单距离 > 司机配置的接单订单距离那么就不允许接单
            if(expectsMileage.compareTo(new BigDecimal(orderDistance)) == 1){
                continue;
            }

            // 5.发送订单给MQ中对应司机的通道
            String driverPullOrderDestination = String.format(Constants.RocketMQ.DRIVER_PULL_ORDER_TOPIC, driverId);
            DriverPullOrderDto driverPullOrderDto = new DriverPullOrderDto();
            driverPullOrderDto.setDistance(distance);
            driverPullOrderDto.setRealOrderAmount(mileagePricingResultDto.getRealOrderAmount());
            driverPullOrderDto.setOrderNo(orderNo);
            driverPullOrderDto.setStartPlace(costomerCreateOrderDto.getStartPlace());
            driverPullOrderDto.setEndPlace(costomerCreateOrderDto.getEndPlace());
            driverPullOrderDto.setExpectsMileage(expectsMileage);
            driverPullOrderDto.setExpectMinutes(costomerCreateOrderDto.getExpectMinutes());
            Message<DriverPullOrderDto> message = MessageBuilder.withPayload(driverPullOrderDto).build();
            SendResult sendResult = rocketMQTemplate.syncSend(driverPullOrderDestination, message);
            if (!SendStatus.SEND_OK.equals(sendResult.getSendStatus())){
                log.error("订单发送MQ消息给司机失败，司机Id为：{}，订单号为：{}", driverId, orderNo);
                continue;
            }
            isSuccess = true;
        }

        // 6.判断标记位是否成功，如果失败提示用户没有匹配到司机
        AssertUtil.isTrue(isSuccess, GlobalExceptionCode.SERVICE_ERROR);

        // 7.发送MQ延迟消息，用于处理订单长时间无人接单的自动取消
        Message<String> message = MessageBuilder.withPayload(orderNo).build();
        SendResult sendResult = rocketMQTemplate.syncSend(Constants.RocketMQ.ORDER_TIMEOUT_CANCEL_TOPIC + ":" + Constants.RocketMQ.ORDER_TIMEOUT_CANCEL_TAG, message, 3000, 5);
        if (!SendStatus.SEND_OK.equals(sendResult.getSendStatus())){
            log.error("订单延迟消息发送失败，订单号为：{}", orderNo);
        }
        String orderInfoKey = String.format(Constants.Redis.ORDER_INFO_KEY, orderNo);
        redisTemplate.opsForValue().set(orderInfoKey, order);

        return orderNo;

    }

    /**
     * 手动取消订单
     * @param orderNo
     */
    @Override
    public void cancelHandle(String orderNo) {
        // 1.参数校验-JSR303
        // 2.业务校验
        // 2.1.订单是否存在
        Order order = super.getOne(new LambdaQueryWrapper<Order>().eq(Order::getOrderNo, orderNo));
        AssertUtil.isNotNull(order, GlobalExceptionCode.SERVICE_ERROR);
        // 2.2.判断此订单是否是当前登录人的订单
        AssertUtil.isEquals(StpUtil.getLoginIdAsString(), order.getCustomerId().toString(), GlobalExceptionCode.SERVICE_ERROR);
        // 2.3.判断订单状态是否为：待接单、已接单、司机已到达
        Integer status = order.getStatus();
        AssertUtil.isTrue(Constants.Order.ORDER_CANCELABLE.contains(status), GlobalExceptionCode.SERVICE_ERROR);

        // 3.业务实现
        // 3.0.如果是待接单什么都不管
        // 3.1.如果订单状态是已接单就罚款总金额的百分之10
        CreateCustomerFineDto createCustomerFineDto = new CreateCustomerFineDto();
        if (Constants.Order.RECEIVED_ORDER.equals(status)){
            // 1.计算要缴纳的金额
            BigDecimal fineAmount = order.getRealOrderAmount().multiply(new BigDecimal("0.1"));
            createCustomerFineDto.setAmount(fineAmount);
        }else if (Constants.Order.DRIVER_ARRIVED.equals(status)){ // 3.2.如果订单状态是司机已到达就罚款总金额的百分之20
            BigDecimal fineAmount = order.getRealOrderAmount().multiply(new BigDecimal("0.2"));
            createCustomerFineDto.setAmount(fineAmount);
        }
        if (createCustomerFineDto.getAmount() != null){
            createCustomerFineDto.setOrderId(order.getId());
            createCustomerFineDto.setCustomerId(StpUtil.getLoginIdAsLong());
            createCustomerFineDto.setRemark("乘客手动取消订单，订单号为："+order.getOrderNo()+"，罚款金额为：" + createCustomerFineDto.getAmount());
            customerFineApi.createCustomerFine(createCustomerFineDto);
        }


        // 3.3.修改订单状态为取消
        order.setStatus(Constants.Order.CUSTOMER_CANCEL);
        super.updateById(order);

        // 3.4.删除Redis中的订单信息
        String orderInfoKey = String.format(Constants.Redis.ORDER_INFO_KEY, orderNo);
        redisTemplate.delete(orderInfoKey);
    }

    // 自动取消订单
    @Override
    public void countEndHandle(String orderNo) {
        // 1.参数校验-JSR303
        // 2.业务校验
        // 2.1.订单是否存在
        Order order = super.getOne(new LambdaQueryWrapper<Order>().eq(Order::getOrderNo, orderNo));
        AssertUtil.isNotNull(order, GlobalExceptionCode.SERVICE_ERROR);
        // 2.2.判断此订单是否是当前登录人的订单
        AssertUtil.isEquals(StpUtil.getLoginIdAsString(), order.getCustomerId().toString(), GlobalExceptionCode.SERVICE_ERROR);
        // 2.3.判断订单状态是否为：待接单、已接单、司机已到达
        Integer status = order.getStatus();
        AssertUtil.isTrue(Constants.Order.WAIT_ORDER.equals(status), GlobalExceptionCode.SERVICE_ERROR);

        // 3.业务实现
        // 3.1.修改订单状态为取消
        order.setStatus(Constants.Order.CUSTOMER_CANCEL);
        super.updateById(order);

        // 3.2.删除Redis中的订单信息
        String orderInfoKey = String.format(Constants.Redis.ORDER_INFO_KEY, orderNo);
        redisTemplate.delete(orderInfoKey);
    }

    /**
     * 查询乘客进行中的订单
     * @return
     */
    @Override
    public Order getCustomerInPorcessOrder() {
        // 根据乘客Id、进行中状态集合
        Order progressOrder = super.getOne(new LambdaQueryWrapper<Order>()
                .eq(Order::getCustomerId, StpUtil.getLoginIdAsLong())
                .in(Order::getStatus, Constants.Order.CUSTOMER_ORDER_IN_PROGRESS));
        return progressOrder;
    }

    /**
     * 根据订单编号查询订单
     * @param orderNo
     * @return
     */
    @Override
    public Order selectOrderNo(String orderNo) {
        return super.getOne(new LambdaQueryWrapper<Order>().eq(Order::getOrderNo, orderNo));
    }

    /**
     * 司机拉取订单信息
     * @return
     */
    @Override
    public List<DriverPullOrderDto> driverPullOrder() {
        try {
            String loginId = StpUtil.getLoginIdAsString();

            // 1.订阅topic，得到此topic下的四个队列
            Set<MessageQueue> messageQueues = pullConsumer.fetchSubscribeMessageQueues("driver-pull-order-topic");
            // 2.准备返回集合
            List<DriverPullOrderDto> result = new ArrayList<>();

            // 3.遍历四个队列，因为我们的订单消息可能在任何队列中
            for (MessageQueue messageQueue : messageQueues) {
                // 1.通过偏移量来获取消息
                long offset = pullConsumer.fetchConsumeOffset(messageQueue, true);
                // 2.拉取消息，需要指定tags来区分司机
                PullResult pullResult = pullConsumer.pull(messageQueue, loginId + "-tag", offset, 5);
                // 3.判断是否找到消息，如果找到那么获取消息内容
                if (pullResult != null && pullResult.getPullStatus().equals(PullStatus.FOUND)) {
                    // 1.获取找到的消息集合
                    List<MessageExt> messageExtList = pullResult.getMsgFoundList();
                    // 2.判断消息集合是否为空
                    if(messageExtList == null || messageExtList.size() == 0)continue;
                    // 3.遍历消息集合，处理消息
                    for (MessageExt messageExt : messageExtList) {
                        // 1.拿到消息内容，转换为订单对象
                        String message = new String(messageExt.getBody(), StandardCharsets.UTF_8);
                        DriverPullOrderDto driverPullOrderDto = JSONUtil.toBean(JSONUtil.toJsonStr(message), DriverPullOrderDto.class);
                        //添加到结果列表
                        result.add(driverPullOrderDto);
                        log.info("获取订单消息 {}", driverPullOrderDto);
                    }
                }
                // 4.重要：修改消息的消费位置，如果位置不后移，消息会一直被消费
                pullConsumer.updateConsumeOffset(messageQueue, pullResult.getNextBeginOffset());
            }
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 司机抢单
     * @param orderNo
     * @return
     */
    @Override
    public Order orderGrab(String orderNo) {
        // 1.参数校验-JSR303
        // 2.业务校验
        // 2.1.判断订单是否存在
        String orderInfoKey = String.format(Constants.Redis.ORDER_INFO_KEY, orderNo);
        Object orderObj = redisTemplate.opsForValue().get(orderInfoKey);
        AssertUtil.isNotNull(orderObj, GlobalExceptionCode.SERVICE_ERROR);

        // 2.2.判断订单状态是否为待接单
        Order order = JSONUtil.toBean(JSONUtil.toJsonStr(orderObj), Order.class);
        AssertUtil.isEquals(order.getStatus(), Constants.Order.WAIT_ORDER, GlobalExceptionCode.SERVICE_ERROR);

        // 3.业务实现
        // 3.1.加分布式锁
        RLock lock = redissonClient.getLock(orderNo);

        try {
            // lock()：线程会死等
            // tryLock()：尝试获取锁，获取不到算求
            boolean isLock = lock.tryLock();

            AssertUtil.isTrue(isLock, GlobalExceptionCode.SERVICE_ERROR);

            // 判断司机是否有进行中的订单
            Order progressOrder = super.getOne(new LambdaQueryWrapper<Order>()
                    .eq(Order::getDriverId, StpUtil.getLoginIdAsLong())
                    .in(Order::getStatus, Constants.Order.DRIVER_ORDER_IN_PROGRESS));
            AssertUtil.isNull(progressOrder, GlobalExceptionCode.SERVICE_ERROR);

            // 3.2.再次判断订单
            // 判断订单是否存在
            orderInfoKey = String.format(Constants.Redis.ORDER_INFO_KEY, orderNo);
            orderObj = redisTemplate.opsForValue().get(orderInfoKey);
            AssertUtil.isNotNull(orderObj, GlobalExceptionCode.SERVICE_ERROR);

            // 判断订单状态是否为待接单
            order = JSONUtil.toBean(JSONUtil.toJsonStr(orderObj), Order.class);
            AssertUtil.isEquals(order.getStatus(), Constants.Order.WAIT_ORDER, GlobalExceptionCode.SERVICE_ERROR);

            // 3.3.修改订单信息
            Date date = new Date();
            order.setUpdateTime(date);
            long loginId = StpUtil.getLoginIdAsLong();
            order.setDriverId(loginId);
            LoginInfoDto loginInfo = loginTemplate.getLoginInfo(loginId);
            order.setDriverName(loginInfo.getName());
            order.setDriverPhone(loginInfo.getPhone());
            order.setDriverPhoto(loginInfo.getAvatar());
            order.setAcceptTime(date);
            order.setStatus(Constants.Order.RECEIVED_ORDER);
            super.updateById(order);

            // 3.4.删除Redis中的订单信息
            redisTemplate.delete(orderInfoKey);

            return order;
        } finally {
            if(lock.isLocked()){
                lock.unlock();
            }
        }
    }

    @Override
    public Order arrive(String orderNo) {
        // 1.参数校验-JSR303
        // 2.业务校验
        // 2.1.订单是否存在
        Order order = selectOrderNo(orderNo);
        AssertUtil.isNotNull(order, GlobalExceptionCode.SERVICE_ERROR);
        // 2.2.订单状态是否是已接单
        AssertUtil.isEquals(order.getStatus(), Constants.Order.RECEIVED_ORDER, GlobalExceptionCode.SERVICE_ERROR);

        // 3.业务实现
        // 3.1.修改订单状态，到达时间，修改时间
        order.setStatus(Constants.Order.DRIVER_ARRIVED);
        Date date = new Date();
        order.setArriveTime(date);
        order.setUpdateTime(date);
        super.updateById(order);

        return order;
    }

    /**
     * 司机获取进行中的订单
     * @return
     */
    @Override
    public Order getOrderInProgress() {
        return super.getOne(new LambdaQueryWrapper<Order>()
                .eq(Order::getDriverId, StpUtil.getLoginIdAsLong())
                .in(Order::getStatus, Constants.Order.DRIVER_ORDER_IN_PROGRESS));
    }

    @Override
    public OrderLocationInfoVo location(String orderNo) {
        // 1.参数校验-JSR303
        // 2.业务校验
        // 2.1.订单是否存在
        Order order = selectOrderNo(orderNo);
        AssertUtil.isNotNull(order, GlobalExceptionCode.SERVICE_ERROR);
        OrderLocationInfoVo orderLocationInfoVo = OrderLocationInfoVo.builder()
                .status(order.getStatus())
                .startPlaceLongitude(order.getStartPlaceLongitude())
                .startPlaceLatitude(order.getStartPlaceLatitude())
                .endPlaceLongitude(order.getEndPlaceLongitude())
                .endPlaceLatitude(order.getEndPlaceLatiude())
                .build();
        return orderLocationInfoVo;
    }

    /**
     * 司机开始代驾
     * @param orderNo
     * @return
     */
    @Override
    public Order startDriving(String orderNo) {
        // 1.参数校验-JSR303
        // 2.业务校验
        // 2.1.订单是否存在
        Order order = selectOrderNo(orderNo);
        AssertUtil.isNotNull(order, GlobalExceptionCode.SERVICE_ERROR);
        // 2.2.订单状态是否为到达代驾点
        AssertUtil.isEquals(order.getStatus(), Constants.Order.DRIVER_ARRIVED, GlobalExceptionCode.SERVICE_ERROR);
        // 2.3.该订单是否是当前司机的
        AssertUtil.isEquals(order.getDriverId().toString(), StpUtil.getLoginIdAsString(), GlobalExceptionCode.SERVICE_ERROR);
        // 3.业务实现
        // 3.1.修改订单信息
        Date date = new Date();
        order.setStartTime(date);
        order.setUpdateTime(date);
        order.setStatus(Constants.Order.START_DRIVING);
        super.updateById(order);
        // 3.2.计算等待时间，修改账单信息
        OrderBill orderBill = orderBillService.selectByOrderNo(orderNo);
        Date arriveTime = order.getArriveTime();
        Long waitingMinute = DateUtil.between(arriveTime, date, DateUnit.MINUTE);
        orderBill.setWaitingMinute(waitingMinute.intValue());
        orderBill.setUpdateTime(date);
        orderBillService.updateById(orderBill);

        return order;
    }


    /**
     * 查询订单详情
     * @param orderNo
     * @return
     */
    @Override
    public OrderDetailVo orderDetail(String orderNo) {
        // 1.参数校验-JSR303
        // 2.业务校验
        // 2.1.订单是否存在
        Order order = selectOrderNo(orderNo);
        AssertUtil.isNotNull(order, GlobalExceptionCode.SERVICE_ERROR);

        OrderBill orderBill = orderBillService.selectByOrderNo(orderNo);
        AssertUtil.isNotNull(orderBill, GlobalExceptionCode.SERVICE_ERROR);

        OrderDetailVo orderDetailVo = new OrderDetailVo();

        BeanUtil.copyProperties(order, orderDetailVo, false);
        BeanUtil.copyProperties(orderBill, orderDetailVo, false);
        return orderDetailVo;
    }

    /**
     * 司机结束代驾
     * @param orderNo
     * @return
     */
    @Override
    public String endDrivingHandle(String orderNo) {
        // 1.参数校验-JSR303
        // 2.业务校验
        // 2.1.订单是否存在
        Order order = selectOrderNo(orderNo);
        AssertUtil.isNotNull(order, GlobalExceptionCode.SERVICE_ERROR);
        // 2.2.订单状态是否为开始代驾
        AssertUtil.isEquals(order.getStatus(), Constants.Order.START_DRIVING, GlobalExceptionCode.SERVICE_ERROR);
        // 2.3.订单是否为当前司机订单
        AssertUtil.isEquals(StpUtil.getLoginIdAsString(), order.getDriverId().toString(), GlobalExceptionCode.SERVICE_ERROR);

        // 2.4.账单是否存在
        OrderBill orderBill = orderBillService.selectByOrderNo(orderNo);
        AssertUtil.isNotNull(orderBill, GlobalExceptionCode.SERVICE_ERROR);

        // 3.业务实现
        // 3.1.根据订单号调用大数据服务，获取到当前订单的所有经纬度
        R<List<DriverPoint>> locationsByOrderNo = appDriverApi.getLocationsByOrderNo(orderNo);
        AssertUtil.isTrue(locationsByOrderNo.isSuccess(), GlobalExceptionCode.SERVICE_ERROR);
        List<DriverPoint> locations = locationsByOrderNo.getData();
        // 3.2.调用腾讯地图批量测距功能，测算当前订单真实距离
        Integer totalDistance = 0;

        Integer totalDuration = 0;
        for (int i = 0; i < locations.size() - 1; i++) {
            DriverPoint from = locations.get(i);
            DriverPoint to = locations.get(i + 1);
            DistanceMatrixResultDto distanceMatrix = txMapTemplate.getDistanceMatrix("driving",
                    new Point(new BigDecimal(from.getLongitude()), new BigDecimal(from.getLatitude())),
                    new Point(new BigDecimal(to.getLongitude()), new BigDecimal(to.getLatitude())));
            totalDistance += distanceMatrix.getDistance();
            totalDuration += distanceMatrix.getDuration();
        }

        AssertUtil.isFalse(totalDistance == 0 || totalDuration == 0, GlobalExceptionCode.SERVICE_ERROR);

        // 3.3.调用规则服务算真实里程以及真实价格等
        R<MileagePricingResultDto> mileagePricingResultDtoR = ruleApi.mileagePricing(new MileagePricingDto()
                .setMileage(new BigDecimal(totalDistance))
                .setPricingTime(order.getCreateTime())
                .setWaitingMinute(orderBill.getWaitingMinute()));
        AssertUtil.isTrue(mileagePricingResultDtoR.isSuccess(), GlobalExceptionCode.SERVICE_ERROR);
        MileagePricingResultDto resultDtoRData = mileagePricingResultDtoR.getData();
        // 3.4.设置真实里程、价格等信息到order和orderBill中
        Date nowDate = new Date();
        order.setStatus(Constants.Order.END_DRIVING);
        order.setUpdateTime(nowDate);
        order.setEndTime(nowDate);
        order.setExpectMinutes(totalDuration);
        order.setRealOrderAmount(resultDtoRData.getRealOrderAmount());
        super.updateById(order);

        orderBill.setUpdateTime(nowDate);
        orderBill.setWaitingAmount(resultDtoRData.getWaitingAmount());
        orderBill.setMileageAmount(resultDtoRData.getMileageAmount());
        orderBill.setReturnAmont(resultDtoRData.getReturnAmont());
        orderBill.setRealOrderAmount(resultDtoRData.getRealOrderAmount());
        orderBillService.updateById(orderBill);

        return orderNo;
    }

    private OrderBill createOrderBill(Order order, MileagePricingResultDto mileagePricingResultDto) {
        OrderBill orderBill = new OrderBill();
        orderBill.setOrderId(order.getId());
        orderBill.setRealOrderAmount(mileagePricingResultDto.getRealOrderAmount());
        orderBill.setBaseMileage(mileagePricingResultDto.getBaseMileage());
        orderBill.setBaseMileageAmount(mileagePricingResultDto.getBaseMileageAmount());
        orderBill.setExceedBaseMileageAmount(mileagePricingResultDto.getExceedBaseMileageAmount());
        orderBill.setMileageAmount(mileagePricingResultDto.getMileageAmount());
        orderBill.setFreeBaseWaitingMinute(mileagePricingResultDto.getFreeBaseWaitingMinute());
        orderBill.setFreeBaseReturnMileage(mileagePricingResultDto.getFreeBaseReturnMileage());
        orderBill.setExceedBaseReturnEveryKmAmount(mileagePricingResultDto.getExceedBaseReturnEveryKmAmount());
        orderBill.setReturnAmont(mileagePricingResultDto.getReturnAmont());
        orderBill.setOrderNo(order.getOrderNo());
        orderBill.setCreateTime(new Date());
        orderBill.setExceedEveryMinuteAmount(mileagePricingResultDto.getExceedEveryMinuteAmount());
        orderBillMapper.insert(orderBill);
        return orderBill;
    }

    private Order createOrder(MileagePricingResultDto mileagePricingResultDto, CostomerCreateOrderDto costomerCreateOrderDto) {
        Order order = new Order();
        order.setOrderNo(UUID.randomUUID().toString());
        long loginId = StpUtil.getLoginIdAsLong();
        order.setCustomerId(loginId);
        order.setStartPlace(costomerCreateOrderDto.getStartPlace());
        order.setEndPlace(costomerCreateOrderDto.getEndPlace());
        order.setExpectsMileage(costomerCreateOrderDto.getExpectsMileage());
        order.setReturnMileage(costomerCreateOrderDto.getExpectsMileage());
        order.setExpectsOrderAmount(mileagePricingResultDto.getMileageAmount());
        order.setRealOrderAmount(mileagePricingResultDto.getRealOrderAmount());
        order.setCarPlate(costomerCreateOrderDto.getCarPlate());
        order.setCarType(costomerCreateOrderDto.getCarType());
        order.setStatus(Constants.Order.WAIT_ORDER);
        order.setExpectMinutes(costomerCreateOrderDto.getExpectMinutes());
        order.setStartPlaceLongitude(costomerCreateOrderDto.getStartPlaceLongitude());
        order.setStartPlaceLatitude(costomerCreateOrderDto.getStartPlaceLatitude());
        order.setEndPlaceLongitude(costomerCreateOrderDto.getEndPlaceLongitude());
        order.setEndPlaceLatiude(costomerCreateOrderDto.getEndPlaceLatiude());
        order.setIncentiveFeeStatus(false);

        // 获取用户信息
        LoginInfoDto loginInfoDto = loginTemplate.getLoginInfo(loginId);
        order.setCustomerName(loginInfoDto.getName());
        order.setCustomerPhone(loginInfoDto.getPhone());
        order.setCustomerPhoto(loginInfoDto.getAvatar());

        order.setCreateTime(new Date());
        super.save(order);
        return order;
    }
}

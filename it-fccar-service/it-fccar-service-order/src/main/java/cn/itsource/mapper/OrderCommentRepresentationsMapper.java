package cn.itsource.mapper;

import cn.itsource.pojo.domain.OrderCommentRepresentations;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 差评申述 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface OrderCommentRepresentationsMapper extends BaseMapper<OrderCommentRepresentations> {

}

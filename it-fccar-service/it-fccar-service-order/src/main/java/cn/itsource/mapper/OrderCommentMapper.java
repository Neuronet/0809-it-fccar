package cn.itsource.mapper;

import cn.itsource.pojo.domain.OrderComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单评价表 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface OrderCommentMapper extends BaseMapper<OrderComment> {

}

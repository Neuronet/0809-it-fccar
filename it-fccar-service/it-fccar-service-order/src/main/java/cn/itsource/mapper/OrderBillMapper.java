package cn.itsource.mapper;

import cn.itsource.pojo.domain.OrderBill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单账单表 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface OrderBillMapper extends BaseMapper<OrderBill> {

}

package cn.itsource.mapper;

import cn.itsource.pojo.domain.OrderProfitsharing;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 平台司机分账表 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface OrderProfitsharingMapper extends BaseMapper<OrderProfitsharing> {

}

package cn.itsource.mapper;

import cn.itsource.pojo.domain.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-09
 */
public interface OrderMapper extends BaseMapper<Order> {

}

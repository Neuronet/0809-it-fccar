package cn.itsource.controller.app;

import cn.itsource.pojo.domain.Order;
import cn.itsource.pojo.dto.CostomerCreateOrderDto;
import cn.itsource.result.R;
import cn.itsource.service.IOrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Tag(name = "小程序-订单对象",description = "小程序-订单对象")
@RestController
@RequestMapping("/app/order")
public class AppOrderController {

    @Autowired
    public IOrderService orderService;

    @Operation( summary= "司机结束代驾",description = "司机结束代驾接口")
    @PostMapping("/endDrivingHandle/{orderNo}")
    public R endDrivingHandle(@PathVariable("orderNo") String orderNo){
        return R.success(orderService.endDrivingHandle(orderNo));
    }

    @Operation( summary= "根据订单号查询订单详情信息",description = "根据订单号查询订单详情信息接口")
    @GetMapping("/orderDetail/{orderNo}")
    public R getOrderDetailByOrderNo(@PathVariable("orderNo") String orderNo){
        return R.success(orderService.orderDetail(orderNo));
    }

    @Operation( summary= "根据订单号查询订单信息",description = "根据订单号查询订单信息接口")
    @PostMapping("/orderByOrderNo/{orderNo}")
    public R getOrderByOrderNo(@PathVariable("orderNo") String orderNo){
        return R.success(orderService.selectOrderNo(orderNo));
    }

    @Operation( summary= "司机开始代驾",description = "司机开始代驾接口")
    @PostMapping("/startDriving/{orderNo}")
    public R startDriving(@PathVariable("orderNo") String orderNo){
        return R.success(orderService.startDriving(orderNo));
    }

    @Operation( summary= "查询订单位置信息",description = "查询订单位置信息接口")
    @GetMapping("/location/{orderNo}")
    public R location(@PathVariable("orderNo") String orderNo){
        return R.success(orderService.location(orderNo));
    }

    @Operation( summary= "司机获取进行中的订单",description = "司机获取进行中的订单接口")
    @GetMapping("/driver/progress")
    public R getOrderInProgress(){
        return R.success(orderService.getOrderInProgress());
    }


    @Operation( summary= "司机到达代驾点",description = "司机到达代驾点接口")
    @PostMapping("/arrive/{orderNo}")
    public R arrive(@PathVariable("orderNo") String orderNo){
        return R.success(orderService.arrive(orderNo));
    }

    @Operation( summary= "司机抢单",description = "司机抢单接口")
    @PostMapping("/orderGrab/{orderNo}")
    public R orderGrab(@PathVariable("orderNo") String orderNo){
        return R.success(orderService.orderGrab(orderNo));
    }

    @Operation( summary= "司机拉取订单信息",description = "司机拉取订单信息接口")
    @GetMapping("/driver/pullOrder")
    public R driverPullOrder(){
        return R.success(orderService.driverPullOrder());
    }

    @Operation( summary= "查询乘客进行中的订单",description = "查询乘客进行中的订单接口")
    @GetMapping("/customer/inPorcess")
    public R getCustomerInPorcessOrder(){
        return R.success(orderService.getCustomerInPorcessOrder());
    }

    @Operation( summary= "乘客手动取消订单",description = "乘客手动取消订单接口")
    @PostMapping("/cancelHandle/{orderNo}")
    public R cancelHandle(@PathVariable("orderNo") String orderNo){
        orderService.cancelHandle(orderNo);
        return R.success();
    }

    @Operation( summary= "系统自动取消订单",description = "系统自动取消订单接口")
    @PostMapping("/countEndHandle/{orderNo}")
    public R countEndHandle(@PathVariable("orderNo") String orderNo){
        orderService.countEndHandle(orderNo);
        return R.success();
    }

    @Operation( summary= "乘客下单",description = "乘客下单接口")
    @Parameter(name = "costomerCreateOrderDto",description = "订单参数对象",required = true)
    @PostMapping("/create")
    public R create(@RequestBody @Valid CostomerCreateOrderDto costomerCreateOrderDto){
        return R.success(orderService.create(costomerCreateOrderDto));
    }

}

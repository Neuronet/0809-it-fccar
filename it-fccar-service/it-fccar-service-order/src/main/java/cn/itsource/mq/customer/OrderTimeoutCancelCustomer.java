package cn.itsource.mq.customer;

import cn.hutool.core.util.StrUtil;
import cn.itsource.constants.Constants;
import cn.itsource.pojo.domain.Order;
import cn.itsource.service.IOrderService;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.Date;

@Component
@RocketMQMessageListener(
        consumerGroup = "order-timeout-Cancel-group",
        topic = Constants.RocketMQ.ORDER_TIMEOUT_CANCEL_TOPIC,
        selectorExpression = Constants.RocketMQ.ORDER_TIMEOUT_CANCEL_TAG
)
public class OrderTimeoutCancelCustomer implements RocketMQListener<MessageExt> {

    @Autowired
    private IOrderService orderService;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 超时取消订单
     * @param messageExt
     */
    @Override
    public void onMessage(MessageExt messageExt) {
        byte[] body = messageExt.getBody();
        String orderNo = new String(body, StandardCharsets.UTF_8);
        if(!StrUtil.isEmpty(orderNo)){
            Order order = orderService.selectOrderNo(orderNo);
            if(order != null && Constants.Order.WAIT_ORDER.equals(order.getStatus())){
                order.setStatus(Constants.Order.CUSTOMER_CANCEL);
                order.setUpdateTime(new Date());
                orderService.updateById(order);
                // 删除Redis中的订单信息
                String orderInfoKey = String.format(Constants.Redis.ORDER_INFO_KEY, orderNo);
                redisTemplate.delete(orderInfoKey);
            }
        }
    }
}

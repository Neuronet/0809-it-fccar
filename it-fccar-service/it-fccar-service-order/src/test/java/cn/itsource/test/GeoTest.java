package cn.itsource.test;

import cn.itsource.constants.Constants;
import cn.itsource.pojo.domain.Point;
import cn.itsource.pojo.dto.DistanceMatrixResultDto;
import cn.itsource.pojo.dto.DriverFromRedisGeoDto;
import cn.itsource.template.RedisGeoTemplate;
import cn.itsource.template.TxMapTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class GeoTest {

    @Autowired
    private RedisGeoTemplate redisGeoTemplate;

    @Autowired
    private TxMapTemplate txMapTemplate;

    @Test
    public void test(){
        redisGeoTemplate.add(Constants.Redis.DRIVER_LOCATION_KEY, "104.06436", "30.54211", "7799");
//        redisGeoTemplate.add(Constants.Redis.DRIVER_LOCATION_KEY, "114.34999", "30.50648", "5577");
//        redisGeoTemplate.add(Constants.Redis.DRIVER_LOCATION_KEY, "114.32999", "30.49648", "5599");
    }

    @Test
    public void testSearch(){
        List<DriverFromRedisGeoDto> driverFromRedisGeoDtos = redisGeoTemplate.searchGeo(Constants.Redis.DRIVER_LOCATION_KEY, "114.34422", "30.50642");
        driverFromRedisGeoDtos.forEach(System.out::println);
    }


    @Test
    public void testMap(){
        Point from = new Point().setLongitude(new BigDecimal("114.39817")).setLatitude(new BigDecimal("30.455988"));

        Point to = new Point().setLongitude(new BigDecimal("114.357399")).setLatitude(new BigDecimal("30.553949"));
        DistanceMatrixResultDto driving = txMapTemplate.getDistanceMatrix("driving", from, to);
    }

}

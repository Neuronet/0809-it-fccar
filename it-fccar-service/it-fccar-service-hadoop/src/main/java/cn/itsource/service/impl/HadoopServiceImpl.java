package cn.itsource.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import cn.itsource.mapper.HadoopMapper;
import cn.itsource.pojo.domain.DriverPoint;
import cn.itsource.service.IHadoopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HadoopServiceImpl implements IHadoopService {

    @Autowired
    private HadoopMapper hadoopMapper;

    @Override
    public List<DriverPoint> getLocationsByOrderNo(String orderNo) {
        return hadoopMapper.selectDriverPointByOrderNo(orderNo);
    }

    @Override
    public void cacheLocation2Hbase(DriverPoint driverPoint) {
        driverPoint.setDriverId(StpUtil.getLoginIdAsLong());
        hadoopMapper.insertDriverLocation(driverPoint);
    }
}

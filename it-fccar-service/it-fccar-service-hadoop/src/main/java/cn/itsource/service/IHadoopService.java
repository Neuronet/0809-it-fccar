package cn.itsource.service;

import cn.itsource.pojo.domain.DriverPoint;

import java.util.List;

public interface IHadoopService {
    List<DriverPoint> getLocationsByOrderNo(String orderNo);

    void cacheLocation2Hbase(DriverPoint driverPoint);
}

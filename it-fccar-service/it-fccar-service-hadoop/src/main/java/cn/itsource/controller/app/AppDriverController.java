package cn.itsource.controller.app;

import cn.itsource.pojo.domain.DriverPoint;
import cn.itsource.result.R;
import cn.itsource.service.IHadoopService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Tag(name = "小程序-大数据-司机对象",description = "小程序-大数据-司机对象")
@RestController
@RequestMapping("/app/hadoop/driver")
public class AppDriverController {

    @Autowired
    private IHadoopService hadoopService;

    @Operation( summary= "获取司机在大数据库中的位置信息",description = "获取司机在大数据库中的位置信息接口")
    @GetMapping("/locations/{orderNo}")
    public R<List<DriverPoint>> getLocationsByOrderNo(@PathVariable("orderNo") String orderNo){
        List<DriverPoint> driverPoints = hadoopService.getLocationsByOrderNo(orderNo);
        return R.success(driverPoints);
    }

    @Operation( summary= "存储司机位置信息到大数据库",description = "存储司机位置信息到大数据库接口")
    @PostMapping("/cache/location")
    public R cacheLocation2Redis(@RequestBody @Valid DriverPoint driverPoint){
        hadoopService.cacheLocation2Hbase(driverPoint);
        return R.success();
    }

}

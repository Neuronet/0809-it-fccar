package cn.itsource.mapper;

import cn.itsource.pojo.domain.DriverPoint;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

public interface HadoopMapper extends BaseMapper<DriverPoint>{

    List<DriverPoint> selectDriverPointByOrderNo(String orderNo);

    void insertDriverLocation(DriverPoint driverPoint);

}

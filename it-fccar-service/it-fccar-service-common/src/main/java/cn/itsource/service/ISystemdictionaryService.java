package cn.itsource.service;

import cn.itsource.pojo.domain.Systemdictionary;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-03
 */
public interface ISystemdictionaryService extends IService<Systemdictionary> {

}

package cn.itsource.service;

import cn.itsource.pojo.domain.Systemdictionaryitem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-03
 */
public interface ISystemdictionaryitemService extends IService<Systemdictionaryitem> {

}

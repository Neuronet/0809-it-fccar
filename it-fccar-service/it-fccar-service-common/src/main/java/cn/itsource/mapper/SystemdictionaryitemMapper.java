package cn.itsource.mapper;

import cn.itsource.pojo.domain.Systemdictionaryitem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-03
 */
public interface SystemdictionaryitemMapper extends BaseMapper<Systemdictionaryitem> {

}

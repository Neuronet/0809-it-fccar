package cn.itsource.mapper;

import cn.itsource.pojo.domain.Systemdictionary;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2024-01-03
 */
public interface SystemdictionaryMapper extends BaseMapper<Systemdictionary> {

}

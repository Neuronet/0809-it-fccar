package cn.itsource.controller.app;


import cn.itsource.constants.GlobalExceptionCode;
import cn.itsource.exception.GlobalException;
import cn.itsource.pojo.domain.Systemdictionary;
import cn.itsource.result.R;
import cn.itsource.template.CosTemplate;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.InputStream;

@Tag(name = "文件上传",description = "文件上传")
@RestController
@RequestMapping("/app/file")
@Validated
public class FileController {

    @Autowired
    private CosTemplate cosTemplate;

    @Operation( summary= "Cos文件上传",description = "Cos文件上传接口")
    @Parameter(name = "systemdictionary",description = "保存的对象",required = true)
    @PostMapping("/cos-upload")
    public R save(@NotNull(message = "文件不能为空！") MultipartFile file) {
        try {
            InputStream inputStream = file.getInputStream();
            if (inputStream == null || file.getSize() == 0){
                throw new GlobalException(GlobalExceptionCode.SERVICE_ERROR);
            }
            // 1.拿文件原始名称
            String originalFilename = file.getOriginalFilename();
            // 2.上传文件
            String fileUrl = cosTemplate.upload(inputStream, originalFilename);
            // 3.响应文件地址
            return R.success(fileUrl);
        } catch (IOException e) {
            throw new GlobalException(GlobalExceptionCode.SERVICE_ERROR);
        }
    }

}

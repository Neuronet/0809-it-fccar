package cn.itsource.service;

import cn.itsource.pojo.domain.DriverAuthMaterial;
import cn.itsource.pojo.dto.AuthRealNameDto;
import cn.itsource.pojo.dto.CacheLocation2GeoDto;
import cn.itsource.pojo.dto.CacheLocation2RedisDto;
import cn.itsource.pojo.dto.CancelAuthRealNameDto;
import cn.itsource.pojo.vo.DriverWorkbeanchVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 司机实名资料 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
public interface IDriverAuthMaterialService extends IService<DriverAuthMaterial> {

    void authRealName(AuthRealNameDto authRealNameDto);

    void cancelAuthRealName(CancelAuthRealNameDto cancelAuthRealNameDto);

    DriverAuthMaterial getDriverAuthMaterial();

    DriverWorkbeanchVo getWorkbeanchData();

    void online();

    void offline();

    void cacheLocation2Geo(CacheLocation2GeoDto cacheLocation2GeoDto);

    void cacheLocation2Redis(CacheLocation2RedisDto cacheLocation2RedisDto);

    CacheLocation2RedisDto getLocationByOrderNo(String orderNo);
}

package cn.itsource.service;

import cn.itsource.pojo.domain.DriverWalletFlow;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 钱包流水 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
public interface IDriverWalletFlowService extends IService<DriverWalletFlow> {

}

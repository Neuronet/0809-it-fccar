package cn.itsource.service.impl;

import cn.itsource.pojo.domain.DriverAggrement;
import cn.itsource.mapper.DriverAggrementMapper;
import cn.itsource.service.IDriverAggrementService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 司机的协议签署 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
@Service
public class DriverAggrementServiceImpl extends ServiceImpl<DriverAggrementMapper, DriverAggrement> implements IDriverAggrementService {

}

package cn.itsource.service.impl;

import cn.itsource.pojo.domain.DriverWallet;
import cn.itsource.mapper.DriverWalletMapper;
import cn.itsource.service.IDriverWalletService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 * 司机的钱包 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
@Service
public class DriverWalletServiceImpl extends ServiceImpl<DriverWalletMapper, DriverWallet> implements IDriverWalletService {

    @Override
    public void initDriverWallet(Long id) {
        DriverWallet driverWallet = new DriverWallet();
        driverWallet.setId(id);
        driverWallet.setCreateTime(new Date());
        super.save(driverWallet);
    }
}

package cn.itsource.service.impl;

import cn.itsource.pojo.domain.DriverSetting;
import cn.itsource.mapper.DriverSettingMapper;
import cn.itsource.service.IDriverSettingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 * 司机配置 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
@Service
public class DriverSettingServiceImpl extends ServiceImpl<DriverSettingMapper, DriverSetting> implements IDriverSettingService {

    @Override
    public void initDriverSetting(Long id) {
        DriverSetting driverSetting = new DriverSetting();
        driverSetting.setId(id);
        driverSetting.setCreateTime(new Date());
        driverSetting.setOrderDistance(50);
        super.save(driverSetting);
    }
}

package cn.itsource.service;

import cn.itsource.pojo.domain.DriverMaterialAuthLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 材料审核日志 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
public interface IDriverMaterialAuthLogService extends IService<DriverMaterialAuthLog> {

}

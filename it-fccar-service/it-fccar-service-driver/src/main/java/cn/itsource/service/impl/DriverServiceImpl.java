package cn.itsource.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.http.HttpUtil;
import cn.itsource.constants.Constants;
import cn.itsource.constants.GlobalExceptionCode;
import cn.itsource.feign.LoginApi;
import cn.itsource.pojo.domain.Driver;
import cn.itsource.mapper.DriverMapper;
import cn.itsource.pojo.domain.DriverAuthMaterial;
import cn.itsource.pojo.dto.LoginDto;
import cn.itsource.pojo.dto.WechatRegisterDto;
import cn.itsource.result.R;
import cn.itsource.service.*;
import cn.itsource.template.WechatTemplate;
import cn.itsource.utils.AssertUtil;
import cn.itsource.utils.BitStatesUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 司机对象 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
@Service
public class DriverServiceImpl extends ServiceImpl<DriverMapper, Driver> implements IDriverService {

    @Autowired
    private IDriverSettingService driverSettingService;

    @Autowired
    private IDriverSummaryService driverSummaryService;

    @Autowired
    private IDriverWalletService driverWalletService;

    @Autowired
    private IDriverAuthMaterialService driverAuthMaterialService;

    @Autowired
    private LoginApi loginApi;

    @Autowired
    private WechatTemplate wechatTemplate;

    /**
     * 保存很多次配置
     *  1.save()
     *  2.update() ? save()?
     * 微信注册
     * @param wechatRegisterDto
     * 1.参数校验-JSR303
     * 2.业务校验
     * 3.业务实现
     * 有些表数据是需要保存主表时，初始化出来的
     */
    @Override
    //@Transactional // 保证在一个方法中的所有数据库操作保持一致，只能保证此方法中操作的同一数据库回滚
    //@GlobalTransactional(name = "driver-wechat-register",rollbackFor = Exception.class)
    public void wechatRegister(WechatRegisterDto wechatRegisterDto) {
        // 在此处会有SpringAOP自动开启事务 begin;

        // 1.参数校验-JSR303
        // 2.业务校验
        // 获取openId
        String openId = wechatTemplate.code2OpenId(wechatRegisterDto.getOpenIdCode());
        // 2.1.判断此用户是否已经注册
        // new LambdaQueryWrapper<Driver>()：条件构造器对象
        // eq()：等于
        // Driver::getOpenId：我要以openid字段作为条件
        Driver driver = super.getOne(new LambdaQueryWrapper<Driver>().eq(Driver::getOpenId, openId));
        AssertUtil.isNull(driver, GlobalExceptionCode.PARAM_PHONE_EXIST);

        // 3.业务实现
        // 3.0.获取用户手机号
        //String phone = wechatTemplate.code2Phone(wechatRegisterDto.getPhoneCode());

        // 3.1.保存司机表
        Long id = saveDriver(openId, "18780084485");
        // 3.2.初始化司机配置表
        driverSettingService.initDriverSetting(id);
        // 3.3.初始化司机统计表
        driverSummaryService.initDriverSummary(id);
        // 3.4.初始化司机钱包表
        driverWalletService.initDriverWallet(id);
        // 3.5.保存登录表
        LoginDto loginDto = new LoginDto();
        loginDto.setId(id);
        loginDto.setType(Constants.Login.TYPE_DRIVER);
        loginDto.setOpenId(openId);
        loginDto.setPhone("18780084485");
        // 发http请求
        R result = loginApi.saveLogin(loginDto);
        AssertUtil.isTrue(result.isSuccess(), GlobalExceptionCode.WECHAT_REGISTER_ERROR);
        // 在此处会有SpringAOP自动提交或者回滚事务  commit;/rollback;
    }

    /**
     * 司机实名认证
     * @param driverAuthMaterial
     */
    @Override
    public void realNameAuth(DriverAuthMaterial driverAuthMaterial) {
        // 1.参数校验-JSR303
        // 2.业务校验
        long loginId = StpUtil.getLoginIdAsLong();
        // 2.1.用户是否有正在审核的实名信息或者是已经审核通过的实名信息
        List<DriverAuthMaterial> materialList = driverAuthMaterialService.list(new LambdaQueryWrapper<DriverAuthMaterial>()
                .eq(DriverAuthMaterial::getDriverId, loginId)
                .in(DriverAuthMaterial::getRealAuthStatus, Constants.RealAuth.VERIFYING, Constants.RealAuth.APPROVED));
        AssertUtil.isTrue(CollectionUtil.isEmpty(materialList), GlobalExceptionCode.SERVICE_ERROR);

        // 3.业务实现
        // 3.1.保存实名认证信息
        driverAuthMaterial.setRealAuthStatus(Constants.RealAuth.VERIFYING);
        driverAuthMaterial.setCreateTime(new Date());
        driverAuthMaterial.setDriverId(loginId);
        driverAuthMaterialService.save(driverAuthMaterial);
        // 3.2.修改司机的位状态
        Driver driver = getById(loginId);
        driver.setBitState(BitStatesUtil.addState(driver.getBitState(), BitStatesUtil.OP_REAL_AUTHENTICATING));
        updateById(driver);
    }

    /**
     * 保存司机信息
     * @param openid
     */
    private Long saveDriver(String openid, String phone) {
        Driver driver = new Driver();
        driver.setPhone(phone);
        driver.setCreateTime(new Date());
        driver.setOpenId(openid);
        driver.setBitState(0L);
        // mybatis的新增方法会把生成的主键返回给你传递给他的对象的id字段
        super.save(driver);
        return driver.getId();
    }
}

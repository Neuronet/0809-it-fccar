package cn.itsource.service;

import cn.itsource.pojo.domain.DriverWallet;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 司机的钱包 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
public interface IDriverWalletService extends IService<DriverWallet> {

    void initDriverWallet(Long id);
}

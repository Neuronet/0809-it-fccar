package cn.itsource.service;

import cn.itsource.pojo.domain.DriverSetting;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 司机配置 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
public interface IDriverSettingService extends IService<DriverSetting> {

    void initDriverSetting(Long id);
}

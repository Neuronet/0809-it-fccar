package cn.itsource.service;

import cn.itsource.pojo.domain.DriverSummary;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 司机结算对象 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
public interface IDriverSummaryService extends IService<DriverSummary> {

    void initDriverSummary(Long id);
}

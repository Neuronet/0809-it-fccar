package cn.itsource.service.impl;

import cn.itsource.pojo.domain.DriverWalletFlow;
import cn.itsource.mapper.DriverWalletFlowMapper;
import cn.itsource.service.IDriverWalletFlowService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 钱包流水 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
@Service
public class DriverWalletFlowServiceImpl extends ServiceImpl<DriverWalletFlowMapper, DriverWalletFlow> implements IDriverWalletFlowService {

}

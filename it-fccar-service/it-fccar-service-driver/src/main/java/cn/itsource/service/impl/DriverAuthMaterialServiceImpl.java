package cn.itsource.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import cn.itsource.constants.Constants;
import cn.itsource.constants.GlobalExceptionCode;
import cn.itsource.mapper.DriverAuthMaterialMapper;
import cn.itsource.mapper.DriverMapper;
import cn.itsource.mapper.DriverSettingMapper;
import cn.itsource.mapper.DriverSummaryMapper;
import cn.itsource.pojo.domain.*;
import cn.itsource.pojo.dto.*;
import cn.itsource.pojo.vo.DriverWorkbeanchVo;
import cn.itsource.service.IDriverAuthMaterialService;
import cn.itsource.service.IDriverMaterialAuthLogService;
import cn.itsource.service.IDriverService;
import cn.itsource.template.RedisGeoTemplate;
import cn.itsource.utils.AssertUtil;
import cn.itsource.utils.BitStatesUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;


/**
 * <p>
 * 司机实名资料 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
@Service
public class DriverAuthMaterialServiceImpl extends ServiceImpl<DriverAuthMaterialMapper, DriverAuthMaterial> implements IDriverAuthMaterialService {

    @Autowired
    private DriverMapper driverMapper;

    @Autowired
    private IDriverMaterialAuthLogService driverMaterialAuthLogService;

    @Autowired
    private DriverSummaryMapper driverSummaryMapper;

    @Autowired
    private DriverSettingMapper driverSettingMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private RedisGeoTemplate redisGeoTemplate;

    /**
     * 司机实名审核
     * @param authRealNameDto
     */
    @Override
    public void authRealName(AuthRealNameDto authRealNameDto) {
        // 1.参数校验
        // 2.业务校验
        // 2.1.司机实名信息是否存在
        DriverAuthMaterial driverAuthMaterial = getById(authRealNameDto.getId());
        AssertUtil.isNotNull(driverAuthMaterial, GlobalExceptionCode.SERVICE_ERROR);
        // 2.2.司机实名信息是否为审核中
        AssertUtil.isTrue(Constants.RealAuth.VERIFYING.equals(driverAuthMaterial.getRealAuthStatus()), GlobalExceptionCode.SERVICE_ERROR);
        // 3.业务实现
        // 3.1.查询司机信息
        Driver driver = driverMapper.selectById(driverAuthMaterial.getDriverId());
        // 审核成功
        if (authRealNameDto.getApprove()){
            driverAuthMaterial.setRealAuthStatus(Constants.RealAuth.APPROVED);
            driver.setBitState(BitStatesUtil.addState(driver.getBitState(), BitStatesUtil.OP_REAL_AUTHENTICATIONED));
        }else {
            // 审核失败
            driverAuthMaterial.setRealAuthStatus(Constants.RealAuth.VERIFY_FAIL);
            driver.setBitState(BitStatesUtil.removeState(driver.getBitState(), BitStatesUtil.OP_REAL_AUTHENTICATING));
        }
        // 3.2.修改实名信息并保存
        long loginId = StpUtil.getLoginIdAsLong();
        driverAuthMaterial.setAuditUserId(loginId);
        driverAuthMaterial.setAuditTime(new Date());
        String remark = authRealNameDto.getAuditRemark();
        driverAuthMaterial.setAuditRemark(remark);
        updateById(driverAuthMaterial);
        // 3.3.修改司机位状态
        driverMapper.updateById(driver);
        // 3.4.保存审核日志
        DriverMaterialAuthLog driverMaterialAuthLog = new DriverMaterialAuthLog();
        driverMaterialAuthLog.setAuthMaterialId(driverAuthMaterial.getId());
        driverMaterialAuthLog.setRealAuthStatus(driverAuthMaterial.getRealAuthStatus());
        driverMaterialAuthLog.setAuditTime(new Date());
        driverMaterialAuthLog.setAuditUserId(loginId);
        driverMaterialAuthLog.setAuditRemark(remark);
        driverMaterialAuthLog.setCreateTime(new Date());
        driverMaterialAuthLogService.save(driverMaterialAuthLog);
    }

    /**
     * 取消实名审核
     * @param cancelAuthRealNameDto
     */
    @Override
    public void cancelAuthRealName(CancelAuthRealNameDto cancelAuthRealNameDto) {
        // 1.参数校验
        // 2.业务校验
        // 2.1.司机实名信息是否存在
        DriverAuthMaterial driverAuthMaterial = getById(cancelAuthRealNameDto.getId());
        AssertUtil.isNotNull(driverAuthMaterial, GlobalExceptionCode.SERVICE_ERROR);
        // 2.2.司机实名信息是否为审核中
        AssertUtil.isTrue(Constants.RealAuth.APPROVED.equals(driverAuthMaterial.getRealAuthStatus()), GlobalExceptionCode.SERVICE_ERROR);
        // 3.业务实现
        // 3.1.查询司机信息
        Driver driver = driverMapper.selectById(driverAuthMaterial.getDriverId());

        driverAuthMaterial.setRealAuthStatus(Constants.RealAuth.REVOKE);
        driver.setBitState(BitStatesUtil.removeState(driver.getBitState(), BitStatesUtil.OP_REAL_AUTHENTICATIONED));
        driver.setBitState(BitStatesUtil.removeState(driver.getBitState(), BitStatesUtil.OP_REAL_AUTHENTICATING));

        // 3.2.修改实名信息并保存
        long loginId = StpUtil.getLoginIdAsLong();
        driverAuthMaterial.setAuditUserId(loginId);
        driverAuthMaterial.setAuditTime(new Date());
        String remark = cancelAuthRealNameDto.getCancelAuditRemark();
        driverAuthMaterial.setAuditRemark(remark);
        updateById(driverAuthMaterial);
        // 3.3.修改司机位状态
        driverMapper.updateById(driver);
        // 3.4.保存审核日志
        DriverMaterialAuthLog driverMaterialAuthLog = new DriverMaterialAuthLog();
        driverMaterialAuthLog.setAuthMaterialId(driverAuthMaterial.getId());
        driverMaterialAuthLog.setRealAuthStatus(driverAuthMaterial.getRealAuthStatus());
        driverMaterialAuthLog.setAuditTime(new Date());
        driverMaterialAuthLog.setAuditUserId(loginId);
        driverMaterialAuthLog.setAuditRemark(remark);
        driverMaterialAuthLog.setCreateTime(new Date());
        driverMaterialAuthLogService.save(driverMaterialAuthLog);
    }

    /**
     * 查询当前登录人的实名认证信息
     * @return
     */
    @Override
    public DriverAuthMaterial getDriverAuthMaterial() {
        // 1.获取当前登录人Id
        long loginId = StpUtil.getLoginIdAsLong();
        // 查询单条数据，第二个参数false意思是当有多条数据的时候取第一条，默认是true，意思是有多条数据报错
        return getOne(new LambdaQueryWrapper<DriverAuthMaterial>()
                        .eq(DriverAuthMaterial::getDriverId, loginId)
                        .orderByDesc(DriverAuthMaterial::getCreateTime)
                , false);
    }

    /**
     * 查询司机工作台信息
     * @return
     */
    @Override
    public DriverWorkbeanchVo getWorkbeanchData() {
        // 1.查询司机实名信息
        long loginId = StpUtil.getLoginIdAsLong();
        DriverAuthMaterial driverAuthMaterial = getOne(new LambdaQueryWrapper<DriverAuthMaterial>()
                .eq(DriverAuthMaterial::getDriverId, loginId)
                .orderByDesc(DriverAuthMaterial::getCreateTime), false);
        // 2.如果司机实名认证为空 或者 司机实名认证状态不为认证成功，那么都响应未认证
        DriverWorkbeanchVo driverWorkbeanchVo = new DriverWorkbeanchVo();
        if (ObjectUtil.isEmpty(driverAuthMaterial) || !Constants.RealAuth.APPROVED.equals(driverAuthMaterial.getRealAuthStatus())){
            return driverWorkbeanchVo.setRealAuthSuccess(false);
        }

        // 3.查询司机统计信息
        DriverSummary driverSummary = driverSummaryMapper.selectById(loginId);
        BeanUtil.copyProperties(driverSummary, driverWorkbeanchVo, false);

        return driverWorkbeanchVo;
    }

    /**
     * 司机上线
     */
    @Override
    public void online() {
        // 1.业务校验，判断司机配置是否存在
        long loginId = StpUtil.getLoginIdAsLong();
        DriverSetting driverSetting = driverSettingMapper.selectById(loginId);
        AssertUtil.isNotNull(driverSetting, GlobalExceptionCode.SERVICE_ERROR);

        // 2.业务实现，保存司机配置到Redis中
        String onlineKey = String.format(Constants.Redis.DRIVER_ONLINE_KEY, loginId);
        DriverSettingToRedisDto driverSettingToRedisDto = new DriverSettingToRedisDto();
        BeanUtil.copyProperties(driverSetting, driverSettingToRedisDto, true);
        redisTemplate.opsForValue().set(onlineKey, driverSettingToRedisDto, 5000, TimeUnit.MINUTES);
    }

    /**
     * 司机下线
     */
    @Override
    public void offline() {
        // 1.删除司机配置
        long loginId = StpUtil.getLoginIdAsLong();
        String onlineKey = String.format(Constants.Redis.DRIVER_ONLINE_KEY, loginId);
        redisTemplate.delete(onlineKey);

        // 2.删除司机GEO信息
        redisTemplate.opsForGeo().remove(Constants.Redis.DRIVER_LOCATION_KEY, loginId);
    }

    /**
     * 缓存司机经纬度信息
     * @param cacheLocation2GeoDto
     */
    @Override
    public void cacheLocation2Geo(CacheLocation2GeoDto cacheLocation2GeoDto) {
        // 1.参数校验-JSR303
        // 2.业务校验
        String loginId = StpUtil.getLoginIdAsString();
        String onlineKey = String.format(Constants.Redis.DRIVER_ONLINE_KEY, loginId);
        Object o = redisTemplate.opsForValue().get(onlineKey);
        AssertUtil.isNotNull(o, GlobalExceptionCode.SERVICE_ERROR);
        // 3.业务实现
        // 3.1.缓存坐标信息到GEO
        redisGeoTemplate.add(Constants.Redis.DRIVER_LOCATION_KEY, cacheLocation2GeoDto.getLongitude(),
                cacheLocation2GeoDto.getLatitude(), loginId);
        // 3.2.给司机配置加时间
        redisTemplate.expire(onlineKey, 5000, TimeUnit.MINUTES);
    }

    /**
     * 缓存司机位置到Redis
     * @param cacheLocation2RedisDto
     */
    @Override
    public void cacheLocation2Redis(CacheLocation2RedisDto cacheLocation2RedisDto) {
        // 1.参数校验-JSR303
        String key = String.format(Constants.Redis.DRIVER_LOCATION_TO_REDIS_KEY, cacheLocation2RedisDto.getOrderNo());
        redisTemplate.opsForValue().set(key, cacheLocation2RedisDto);
    }

    /**
     * 获取司机位置
     * @param orderNo
     * @return
     */
    @Override
    public CacheLocation2RedisDto getLocationByOrderNo(String orderNo) {
        String key = String.format(Constants.Redis.DRIVER_LOCATION_TO_REDIS_KEY, orderNo);
        Object locationObj = redisTemplate.opsForValue().get(key);
        AssertUtil.isNotNull(locationObj, GlobalExceptionCode.SERVICE_ERROR);
        return JSONUtil.toBean(JSONUtil.toJsonStr(locationObj), CacheLocation2RedisDto.class);
    }
}

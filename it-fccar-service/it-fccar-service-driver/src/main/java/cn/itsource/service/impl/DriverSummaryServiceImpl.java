package cn.itsource.service.impl;

import cn.itsource.pojo.domain.DriverSummary;
import cn.itsource.mapper.DriverSummaryMapper;
import cn.itsource.service.IDriverSummaryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 * 司机结算对象 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
@Service
public class DriverSummaryServiceImpl extends ServiceImpl<DriverSummaryMapper, DriverSummary> implements IDriverSummaryService {

    @Override
    public void initDriverSummary(Long id) {
        DriverSummary driverSummary = new DriverSummary();
        driverSummary.setId(id);
        driverSummary.setCreateTime(new Date());
        super.save(driverSummary);
    }
}

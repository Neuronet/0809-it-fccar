package cn.itsource.service;

import cn.itsource.pojo.domain.Driver;
import cn.itsource.pojo.domain.DriverAuthMaterial;
import cn.itsource.pojo.dto.WechatRegisterDto;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 司机对象 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
public interface IDriverService extends IService<Driver> {

    void wechatRegister(WechatRegisterDto wechatRegisterDto);

    void realNameAuth(DriverAuthMaterial driverAuthMaterial);
}

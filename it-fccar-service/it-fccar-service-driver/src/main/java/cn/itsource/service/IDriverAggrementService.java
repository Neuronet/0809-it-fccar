package cn.itsource.service;

import cn.itsource.pojo.domain.DriverAggrement;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 司机的协议签署 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
public interface IDriverAggrementService extends IService<DriverAggrement> {

}

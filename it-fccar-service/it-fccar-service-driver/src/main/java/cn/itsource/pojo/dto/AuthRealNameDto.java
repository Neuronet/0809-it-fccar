package cn.itsource.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class AuthRealNameDto {

    private Boolean approve;
    private String auditRemark;
    private Long id;

}

package cn.itsource.pojo.dto;

import lombok.Data;

@Data
public class CacheLocation2GeoDto {

    private String longitude;
    private String latitude;

}

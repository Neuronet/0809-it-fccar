package cn.itsource.pojo.dto;

import lombok.Data;

@Data
public class CacheLocation2RedisDto extends CacheLocation2GeoDto{

    private String orderNo;

}

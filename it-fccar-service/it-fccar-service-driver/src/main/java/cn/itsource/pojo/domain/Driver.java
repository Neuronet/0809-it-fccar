package cn.itsource.pojo.domain;

import cn.itsource.constants.Constants;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;


/**
 * <p>
 * 司机对象
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
// 实体类加上这个注解，可以让我们在调用setter 和 getter方法的时候链式编程
@Accessors(chain = true)
// mybatis-plus中标识当前类所属是那张表
@TableName("t_driver")
// swagger注解，说明当前类
@Schema(name = "Driver对象", description = "司机对象")
public class Driver implements Serializable {

    private static final long serialVersionUID=1L;

    // swagger注解：对字段的说明
    @Schema(name = "id", description = "同loginId")
    // 为了防止返回给前端的时候超出长度，转换为字符串返回给前端
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    // @TableId专门标识主键字段的 此注解是mybatis-plus的注解，value说明对应那个字段 type意思是此ID的类型
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;



    @Schema(name = "phone", description = "电话")
    // 标识普通字段的
    @TableField("phone")
    @Pattern(regexp = Constants.CHINA_PHONE_REGEX, message = "手机号格式不正确！") // 手机号校验
    @NotBlank(message = "手机号不能为空！")
    private String phone;

    @Schema(name = "homeAddress", description = "居住地址")
    @TableField("home_address")
    @NotBlank(message = "居住地址不能为空！")
    private String homeAddress;

    @Schema(name = "homeAddressLongitude", description = "居住地址精度")
    @TableField("home_address_longitude")
    private String homeAddressLongitude;

    @Schema(name = "homeAddressLatitude", description = "居住地址维度")
    @TableField("home_address_latitude")
    private String homeAddressLatitude;

    @Schema(name = "archive", description = "面部数据ID")
    @TableField("archive")
    @AssertTrue(message = "面部数据不正确")
    private Boolean archive;

    @Schema(name = "createTime", description = "创建时间")
    @TableField("create_time")
    private Date createTime;

    @Schema(name = "updateTime", description = "修改时间")
    @TableField("update_time")
    private Date updateTime;

    @Schema(name = "deleted", description = "逻辑删除")
    @TableField("deleted")
    @TableLogic // 标识此字段是逻辑删除
    private Boolean deleted;

    @Schema(name = "version", description = "乐观锁")
    @TableField("version")
    @Version // 标识说明此字段是做乐观锁的
    private Integer version;

    @Schema(name = "openId", description = "微信ID")
    @TableField("open_id")
    private String openId;

    @Schema(name = "bitState", description = "位状态")
    @TableField("bit_state")
    private Long bitState;

}

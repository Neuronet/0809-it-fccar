package cn.itsource.pojo.dto;

import lombok.Data;

@Data
public class CancelAuthRealNameDto {

    private String cancelAuditRemark;
    private Long id;

}

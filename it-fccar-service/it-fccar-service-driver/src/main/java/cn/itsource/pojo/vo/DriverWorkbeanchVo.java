package cn.itsource.pojo.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class DriverWorkbeanchVo {

    private Boolean realAuthSuccess = true;

    private Integer driveDuration;
    private Integer todayIncome;
    private Integer tradeOrders;

}

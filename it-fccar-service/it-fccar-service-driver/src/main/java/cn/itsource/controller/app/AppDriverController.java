package cn.itsource.controller.app;

import cn.dev33.satoken.annotation.SaIgnore;
import cn.itsource.pojo.domain.Driver;
import cn.itsource.pojo.domain.DriverAuthMaterial;
import cn.itsource.pojo.dto.CacheLocation2GeoDto;
import cn.itsource.pojo.dto.CacheLocation2RedisDto;
import cn.itsource.pojo.dto.WechatRegisterDto;
import cn.itsource.pojo.query.PageQueryWrapper;
import cn.itsource.result.PageResult;
import cn.itsource.result.R;
import cn.itsource.service.IDriverAuthMaterialService;
import cn.itsource.service.IDriverService;
import cn.itsource.service.IDriverWalletService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

// swagger：API接口文档
// swagger的注解，给当前类写说明
@Tag(name = "小程序-司机对象",description = "小程序-司机对象")
@RestController
@RequestMapping("/app/driver")
public class AppDriverController {

    @Autowired
    public IDriverService driverService;

    @Autowired
    private IDriverAuthMaterialService driverAuthMaterialService;

    @Operation( summary= "缓存司机经纬度信息到Redis",description = "缓存司机经纬度信息到Redis接口")
    @GetMapping("/location/{orderNo}")
    public R getLocationByOrderNo(@PathVariable("orderNo") String orderNo){
        CacheLocation2RedisDto cacheLocation2RedisDto = driverAuthMaterialService.getLocationByOrderNo(orderNo);
        return R.success(cacheLocation2RedisDto);
    }

    @Operation( summary= "缓存司机经纬度信息到Redis",description = "缓存司机经纬度信息到Redis接口")
    @PostMapping("/cache/location")
    public R cacheLocation2Redis(@RequestBody @Valid CacheLocation2RedisDto cacheLocation2RedisDto){
        driverAuthMaterialService.cacheLocation2Redis(cacheLocation2RedisDto);
        return R.success();
    }

    @Operation( summary= "缓存司机经纬度信息到Geo",description = "缓存司机经纬度信息到Geo接口")
    @PostMapping("/cacheLocation2Geo")
    public R cacheLocation2Geo(@RequestBody @Valid CacheLocation2GeoDto cacheLocation2GeoDto){
        driverAuthMaterialService.cacheLocation2Geo(cacheLocation2GeoDto);
        return R.success();
    }

    @Operation( summary= "司机上线",description = "司机上线接口")
    @PostMapping("/online")
    public R online(){
        driverAuthMaterialService.online();
        return R.success();
    }

    @Operation( summary= "司机下线",description = "司机下线接口")
    @PostMapping("/offline")
    public R offline(){
        driverAuthMaterialService.offline();
        return R.success();
    }

    @Operation( summary= "查询司机工作台信息",description = "查询司机工作台信息接口")
    // 对该接口参数的描述
    @GetMapping("/getWorkbeanchData")
    public R getWorkbeanchData(){
        return R.success(driverAuthMaterialService.getWorkbeanchData());
    }

    @Operation( summary= "查询司机实名认证信息",description = "查询司机实名认证信息接口")
    // 对该接口参数的描述
    @GetMapping("/getDriverAuthMaterial")
    public R getDriverAuthMaterial(){
        return R.success(driverAuthMaterialService.getDriverAuthMaterial());
    }

    @Operation( summary= "司机实名认证",description = "司机实名认证接口")
    // 对该接口参数的描述
    @Parameter(name = "driverAuthMaterial",description = "司机实名认证对象",required = true)
    @PostMapping("/realNameAuth")
    public R realNameAuth(@RequestBody @Valid DriverAuthMaterial driverAuthMaterial){
        driverService.realNameAuth(driverAuthMaterial);
        return R.success();
    }

    /**
     *
     * pojo：所有具有字段、并且具有getter、setter方法的对象我们都称为pojo
     *  1.domain：可以有数据库不存在的字段
     *  2.empty：必须跟数据库字段一一对应
     *  3.dto：后端用于接收前端的参数的对象
     *  4.vo：后端响应给前端用于展示数据的对象
     *  5.bo：带Java代码之间传输参数时的对象
     *
     *
     */
    @SaIgnore
    @Operation( summary= "微信注册",description = "微信注册接口")
    // 对该接口参数的描述
    @Parameter(name = "wechatRegisterDto",description = "注册数据对象",required = true)
    @PostMapping("/wechatRegister")
    public R wechatRegister(@RequestBody @Valid WechatRegisterDto wechatRegisterDto){
        driverService.wechatRegister(wechatRegisterDto);
        return R.success();
    }


}

package cn.itsource.mapper;

import cn.itsource.pojo.domain.DriverSetting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 司机配置 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
public interface DriverSettingMapper extends BaseMapper<DriverSetting> {

}

package cn.itsource.mapper;

import cn.itsource.pojo.domain.DriverWalletFlow;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 钱包流水 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
public interface DriverWalletFlowMapper extends BaseMapper<DriverWalletFlow> {

}

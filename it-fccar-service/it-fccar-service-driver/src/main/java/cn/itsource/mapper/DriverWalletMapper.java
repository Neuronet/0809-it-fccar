package cn.itsource.mapper;

import cn.itsource.pojo.domain.DriverWallet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 司机的钱包 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
public interface DriverWalletMapper extends BaseMapper<DriverWallet> {

}

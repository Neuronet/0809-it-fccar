package cn.itsource.mapper;

import cn.itsource.pojo.domain.DriverAggrement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 司机的协议签署 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
public interface DriverAggrementMapper extends BaseMapper<DriverAggrement> {

}

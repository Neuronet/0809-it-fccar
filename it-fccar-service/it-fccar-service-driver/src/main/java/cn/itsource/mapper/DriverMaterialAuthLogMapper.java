package cn.itsource.mapper;

import cn.itsource.pojo.domain.DriverMaterialAuthLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 材料审核日志 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
public interface DriverMaterialAuthLogMapper extends BaseMapper<DriverMaterialAuthLog> {

}

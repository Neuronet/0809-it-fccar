package cn.itsource.mapper;

import cn.itsource.pojo.domain.Driver;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 司机对象 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
public interface DriverMapper extends BaseMapper<Driver> {

}

package cn.itsource.mapper;

import cn.itsource.pojo.domain.DriverAuthMaterial;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 司机实名资料 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-16
 */
public interface DriverAuthMaterialMapper extends BaseMapper<DriverAuthMaterial> {

}

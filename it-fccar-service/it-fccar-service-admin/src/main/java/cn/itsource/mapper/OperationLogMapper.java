package cn.itsource.mapper;

import cn.itsource.pojo.domain.OperationLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 操作日志记录 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface OperationLogMapper extends BaseMapper<OperationLog> {

}

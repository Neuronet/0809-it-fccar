package cn.itsource.mapper;

import cn.itsource.pojo.domain.SystemWalletFlow;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface SystemWalletFlowMapper extends BaseMapper<SystemWalletFlow> {

}

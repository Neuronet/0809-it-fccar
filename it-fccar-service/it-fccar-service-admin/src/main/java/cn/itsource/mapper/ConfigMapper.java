package cn.itsource.mapper;

import cn.itsource.pojo.domain.Config;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 参数配置表 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface ConfigMapper extends BaseMapper<Config> {

}

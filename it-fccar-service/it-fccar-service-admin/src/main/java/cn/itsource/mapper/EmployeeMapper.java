package cn.itsource.mapper;

import cn.itsource.pojo.domain.Employee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface EmployeeMapper extends BaseMapper<Employee> {

}

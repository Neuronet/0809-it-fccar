package cn.itsource.mapper;

import cn.itsource.pojo.domain.SystemReward;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统奖励 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface SystemRewardMapper extends BaseMapper<SystemReward> {

}

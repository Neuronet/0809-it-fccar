package cn.itsource.pojo.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * <p>
 * 系统奖励
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_system_reward")
@Schema(name = "SystemReward对象", description = "系统奖励")
public class SystemReward implements Serializable {

    private static final long serialVersionUID=1L;

    @Schema(name = "id", description = "ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @Schema(name = "userType", description = "用户类型")
    @TableField("user_type")
    private Integer userType;

    @Schema(name = "amount", description = "奖励金额")
    @TableField("amount")
    private BigDecimal amount;

    @Schema(name = "createTime", description = "创建时间")
    @TableField("create_time")
    private Date createTime;

    @Schema(name = "status", description = "0未奖励，1已奖励")
    @TableField("status")
    private Integer status;

    @Schema(name = "updateTime", description = "修改时间")
    @TableField("update_time")
    private Date updateTime;

    @Schema(name = "version", description = "版本号")
    @TableField("version")
    @Version
    private Integer version;

    @Schema(name = "toUserId", description = "奖励人ID")
    @TableField("to_user_id")
    private Long toUserId;

    @Schema(name = "uniqueNo", description = "唯一标识，防止重复放发")
    @TableField("unique_no")
    private String uniqueNo;

    @Schema(name = "notes", description = "备注")
    @TableField("notes")
    private String notes;

}

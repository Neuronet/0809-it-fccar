package cn.itsource.pojo.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * <p>
 * 参数配置表
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_config")
@Schema(name = "Config对象", description = "参数配置表")
public class Config implements Serializable {

    private static final long serialVersionUID=1L;

    @Schema(name = "id", description = "参数主键")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @Schema(name = "configName", description = "参数名称")
    @TableField("config_name")
    private String configName;

    @Schema(name = "configKey", description = "参数键名")
    @TableField("config_key")
    private String configKey;

    @Schema(name = "configValue", description = "参数键值")
    @TableField("config_value")
    private String configValue;

    @Schema(name = "configType", description = "系统内置（Y是 N否）")
    @TableField("config_type")
    private String configType;

    @Schema(name = "createBy", description = "创建者")
    @TableField("create_by")
    private String createBy;

    @Schema(name = "createTime", description = "创建时间")
    @TableField("create_time")
    private Date createTime;

    @Schema(name = "updateBy", description = "更新者")
    @TableField("update_by")
    private String updateBy;

    @Schema(name = "updateTime", description = "更新时间")
    @TableField("update_time")
    private Date updateTime;

    @Schema(name = "remark", description = "备注")
    @TableField("remark")
    private String remark;

}

package cn.itsource.pojo.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * <p>
 * 
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_employee")
@Schema(name = "Employee对象", description = "")
public class Employee implements Serializable {

    private static final long serialVersionUID=1L;

    @Schema(name = "id", description = "")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @Schema(name = "realName", description = "姓名")
    @TableField("real_name")
    private String realName;

    @Schema(name = "tel", description = "电话")
    @TableField("tel")
    private String tel;

    @Schema(name = "email", description = "邮箱")
    @TableField("email")
    private String email;

    @Schema(name = "inputTime", description = "创建时间")
    @TableField("input_time")
    private Date inputTime;

    @Schema(name = "state", description = "状态：0正常，1锁定，2注销")
    @TableField("state")
    private Integer state;

    @Schema(name = "deptId", description = "部门id")
    @TableField("dept_id")
    private Long deptId;

    @Schema(name = "type", description = "员工类型 ， 1平台普通员工 ，2平台客服人员，3平台管理员，")
    @TableField("type")
    private Boolean type;

    @Schema(name = "loginId", description = "")
    @TableField("login_id")
    private Long loginId;

}

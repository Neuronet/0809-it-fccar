package cn.itsource.pojo.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * <p>
 * 
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_system_wallet")
@Schema(name = "SystemWallet对象", description = "")
public class SystemWallet implements Serializable {

    private static final long serialVersionUID=1L;

    @Schema(name = "id", description = "ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @Schema(name = "amount", description = "账户金额")
    @TableField("amount")
    private BigDecimal amount;

    @Schema(name = "totalRecharge", description = "总充值")
    @TableField("total_recharge")
    private BigDecimal totalRecharge;

    @Schema(name = "totalWithdraw", description = "总提现")
    @TableField("total_withdraw")
    private BigDecimal totalWithdraw;

    @Schema(name = "totalRewardDistribution", description = "总奖励发放")
    @TableField("total_reward_distribution")
    private BigDecimal totalRewardDistribution;

}

package cn.itsource.pojo.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * <p>
 * 
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_department")
@Schema(name = "Department对象", description = "")
public class Department implements Serializable {

    private static final long serialVersionUID=1L;

    @Schema(name = "id", description = "")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @Schema(name = "sn", description = "部门编号")
    @TableField("sn")
    private String sn;

    @Schema(name = "name", description = "部门名称")
    @TableField("name")
    private String name;

    @Schema(name = "dirPath", description = "部门的上级分类层级id")
    @TableField("dir_path")
    private String dirPath;

    @Schema(name = "state", description = "部门状态，0正常，1禁用")
    @TableField("state")
    private Integer state;

    @Schema(name = "managerId", description = "部门管理员，关联Employee表id")
    @TableField("manager_id")
    private Long managerId;

    @Schema(name = "parentId", description = "上级部门")
    @TableField("parent_id")
    private Long parentId;

    @Schema(name = "tenantId", description = "部门所属机构(租户)")
    @TableField("tenant_id")
    private Long tenantId;

}

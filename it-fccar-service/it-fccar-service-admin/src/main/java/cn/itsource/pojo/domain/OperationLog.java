package cn.itsource.pojo.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * <p>
 * 操作日志记录
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_operation_log")
@Schema(name = "OperationLog对象", description = "操作日志记录")
public class OperationLog implements Serializable {

    private static final long serialVersionUID=1L;

    @Schema(name = "id", description = "日志主键")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @Schema(name = "title", description = "模块标题")
    @TableField("title")
    private String title;

    @Schema(name = "businessType", description = "业务类型（0其它 1新增 2修改 3删除）")
    @TableField("business_type")
    private Integer businessType;

    @Schema(name = "method", description = "方法名称")
    @TableField("method")
    private String method;

    @Schema(name = "requestMethod", description = "请求方式")
    @TableField("request_method")
    private String requestMethod;

    @Schema(name = "operatorType", description = "操作类别（0其它 1后台用户 2手机端用户）")
    @TableField("operator_type")
    private Integer operatorType;

    @Schema(name = "operName", description = "操作人员")
    @TableField("oper_name")
    private String operName;

    @Schema(name = "deptName", description = "部门名称")
    @TableField("dept_name")
    private String deptName;

    @Schema(name = "operUrl", description = "请求URL")
    @TableField("oper_url")
    private String operUrl;

    @Schema(name = "operIp", description = "主机地址")
    @TableField("oper_ip")
    private String operIp;

    @Schema(name = "operLocation", description = "操作地点")
    @TableField("oper_location")
    private String operLocation;

    @Schema(name = "operParam", description = "请求参数")
    @TableField("oper_param")
    private String operParam;

    @Schema(name = "jsonResult", description = "返回参数")
    @TableField("json_result")
    private String jsonResult;

    @Schema(name = "status", description = "操作状态（0正常 1异常）")
    @TableField("status")
    private Integer status;

    @Schema(name = "errorMsg", description = "错误消息")
    @TableField("error_msg")
    private String errorMsg;

    @Schema(name = "operTime", description = "操作时间")
    @TableField("oper_time")
    private Date operTime;

}

package cn.itsource.service;

import cn.itsource.pojo.domain.SystemReward;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统奖励 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface ISystemRewardService extends IService<SystemReward> {

}

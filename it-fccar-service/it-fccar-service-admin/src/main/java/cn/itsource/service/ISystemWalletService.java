package cn.itsource.service;

import cn.itsource.pojo.domain.SystemWallet;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface ISystemWalletService extends IService<SystemWallet> {

}

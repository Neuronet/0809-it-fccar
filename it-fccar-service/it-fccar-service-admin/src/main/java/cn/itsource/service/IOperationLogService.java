package cn.itsource.service;

import cn.itsource.pojo.domain.OperationLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 操作日志记录 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface IOperationLogService extends IService<OperationLog> {

}

package cn.itsource.service.impl;

import cn.itsource.pojo.domain.SystemRewardHistory;
import cn.itsource.mapper.SystemRewardHistoryMapper;
import cn.itsource.service.ISystemRewardHistoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统奖励 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
@Service
public class SystemRewardHistoryServiceImpl extends ServiceImpl<SystemRewardHistoryMapper, SystemRewardHistory> implements ISystemRewardHistoryService {

}

package cn.itsource.service.impl;

import cn.itsource.pojo.domain.SystemWalletFlow;
import cn.itsource.mapper.SystemWalletFlowMapper;
import cn.itsource.service.ISystemWalletFlowService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
@Service
public class SystemWalletFlowServiceImpl extends ServiceImpl<SystemWalletFlowMapper, SystemWalletFlow> implements ISystemWalletFlowService {

}

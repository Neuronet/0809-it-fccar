package cn.itsource.service;

import cn.itsource.pojo.domain.SystemWalletFlow;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface ISystemWalletFlowService extends IService<SystemWalletFlow> {

}

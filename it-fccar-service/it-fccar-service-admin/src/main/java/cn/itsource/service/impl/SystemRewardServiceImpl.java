package cn.itsource.service.impl;

import cn.itsource.pojo.domain.SystemReward;
import cn.itsource.mapper.SystemRewardMapper;
import cn.itsource.service.ISystemRewardService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统奖励 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
@Service
public class SystemRewardServiceImpl extends ServiceImpl<SystemRewardMapper, SystemReward> implements ISystemRewardService {

}

package cn.itsource.service.impl;

import cn.itsource.pojo.domain.SystemWallet;
import cn.itsource.mapper.SystemWalletMapper;
import cn.itsource.service.ISystemWalletService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
@Service
public class SystemWalletServiceImpl extends ServiceImpl<SystemWalletMapper, SystemWallet> implements ISystemWalletService {

}

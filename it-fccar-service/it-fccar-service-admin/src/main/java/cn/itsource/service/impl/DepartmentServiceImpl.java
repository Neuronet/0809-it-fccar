package cn.itsource.service.impl;

import cn.itsource.pojo.domain.Department;
import cn.itsource.mapper.DepartmentMapper;
import cn.itsource.service.IDepartmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, Department> implements IDepartmentService {

}

package cn.itsource.service;

import cn.itsource.pojo.domain.Config;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 参数配置表 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface IConfigService extends IService<Config> {

}

package cn.itsource.controller.manager;

import cn.itsource.service.IConfigService;
import cn.itsource.pojo.domain.Config;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import javax.validation.Valid;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.itsource.pojo.query.PageQueryWrapper;
import cn.itsource.result.R;
import cn.itsource.result.PageResult;

@Tag(name = "参数配置表",description = "参数配置表")
@RestController
@RequestMapping("/manager/config")
public class ConfigController{

    @Autowired
    public IConfigService configService;

    @Operation( summary= "保存Config",description = "基础对象保存接口")
    @Parameter(name = "config",description = "保存的对象",required = true)
    @PostMapping
    public R save(@RequestBody @Valid Config config){
        return R.success(configService.save(config));
    }

    @Operation( summary= "修改Config",description = "基础对象修改接口")
    @Parameter(name = "config",description = "修改的对象",required = true)
    @PutMapping
    public R update(@RequestBody  @Valid Config config){
        return R.success(configService.updateById(config));
    }

    //删除对象
    @Operation( summary= "删除Config",description = "基础对象删除接口，采用状态删除")
    @Parameter(name = "id",description = "删除的对象ID",required = true)
    @DeleteMapping(value="/{id}")
    public R delete(@PathVariable("id") Long id){
        return R.success(configService.removeById(id));
    }

    //获取对象
    @Operation( summary= "获取Config",description = "基础对象获取接口")
    @Parameter(name = "id",description = "查询的对象ID",required = true)
    @GetMapping(value = "/{id}")
    public R get(@PathVariable("id")Long id){
        return R.success(configService.getById(id));
    }

    //获取列表:PageQueryWrapper作为通用的查询对象
    @Operation( summary= "查询Config列表",description = "基础对象列表查询，不带分页")
    @Parameter(name = "query",description = "查询条件",required = true)
    @PostMapping(value = "/list")
    public R list(@RequestBody PageQueryWrapper<Config> query){
        QueryWrapper<Config> wrapper = new QueryWrapper<>();
        //实体类作为查询条件
        wrapper.setEntity(query.getQuery());
        return R.success(configService.list(wrapper));
    }

    //分页查询
    @Operation( summary= "查询Config分页列表",description = "基础对象列表查询，带分页")
    @Parameter(name = "query",description = "查询条件，需要指定分页条件",required = true)
    @PostMapping(value = "/pagelist")
    public R page(@RequestBody PageQueryWrapper<Config> query){
        //分页查询
        Page<Config> page = new Page<Config>(query.getPage(),query.getRows());
        QueryWrapper<Config> wrapper = new QueryWrapper<>();
        //实体类作为查询条件
        wrapper.setEntity(query.getQuery());
        //分页查询
        page = configService.page(page,wrapper);
        //返回结果
        return R.success(new PageResult<Config>(page.getTotal(),page.getRecords()));
    }

}

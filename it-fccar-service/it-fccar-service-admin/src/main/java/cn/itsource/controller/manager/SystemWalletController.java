package cn.itsource.controller.manager;

import cn.itsource.service.ISystemWalletService;
import cn.itsource.pojo.domain.SystemWallet;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import javax.validation.Valid;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.itsource.pojo.query.PageQueryWrapper;
import cn.itsource.result.R;
import cn.itsource.result.PageResult;

@Tag(name = "",description = "")
@RestController
@RequestMapping("/manager/systemWallet")
public class SystemWalletController{

    @Autowired
    public ISystemWalletService systemWalletService;

    @Operation( summary= "保存SystemWallet",description = "基础对象保存接口")
    @Parameter(name = "systemWallet",description = "保存的对象",required = true)
    @PostMapping
    public R save(@RequestBody @Valid SystemWallet systemWallet){
        return R.success(systemWalletService.save(systemWallet));
    }

    @Operation( summary= "修改SystemWallet",description = "基础对象修改接口")
    @Parameter(name = "systemWallet",description = "修改的对象",required = true)
    @PutMapping
    public R update(@RequestBody  @Valid SystemWallet systemWallet){
        return R.success(systemWalletService.updateById(systemWallet));
    }

    //删除对象
    @Operation( summary= "删除SystemWallet",description = "基础对象删除接口，采用状态删除")
    @Parameter(name = "id",description = "删除的对象ID",required = true)
    @DeleteMapping(value="/{id}")
    public R delete(@PathVariable("id") Long id){
        return R.success(systemWalletService.removeById(id));
    }

    //获取对象
    @Operation( summary= "获取SystemWallet",description = "基础对象获取接口")
    @Parameter(name = "id",description = "查询的对象ID",required = true)
    @GetMapping(value = "/{id}")
    public R get(@PathVariable("id")Long id){
        return R.success(systemWalletService.getById(id));
    }

    //获取列表:PageQueryWrapper作为通用的查询对象
    @Operation( summary= "查询SystemWallet列表",description = "基础对象列表查询，不带分页")
    @Parameter(name = "query",description = "查询条件",required = true)
    @PostMapping(value = "/list")
    public R list(@RequestBody PageQueryWrapper<SystemWallet> query){
        QueryWrapper<SystemWallet> wrapper = new QueryWrapper<>();
        //实体类作为查询条件
        wrapper.setEntity(query.getQuery());
        return R.success(systemWalletService.list(wrapper));
    }

    //分页查询
    @Operation( summary= "查询SystemWallet分页列表",description = "基础对象列表查询，带分页")
    @Parameter(name = "query",description = "查询条件，需要指定分页条件",required = true)
    @PostMapping(value = "/pagelist")
    public R page(@RequestBody PageQueryWrapper<SystemWallet> query){
        //分页查询
        Page<SystemWallet> page = new Page<SystemWallet>(query.getPage(),query.getRows());
        QueryWrapper<SystemWallet> wrapper = new QueryWrapper<>();
        //实体类作为查询条件
        wrapper.setEntity(query.getQuery());
        //分页查询
        page = systemWalletService.page(page,wrapper);
        //返回结果
        return R.success(new PageResult<SystemWallet>(page.getTotal(),page.getRecords()));
    }

}

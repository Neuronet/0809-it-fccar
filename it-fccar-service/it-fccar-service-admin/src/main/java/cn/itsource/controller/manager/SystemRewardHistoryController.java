package cn.itsource.controller.manager;

import cn.itsource.service.ISystemRewardHistoryService;
import cn.itsource.pojo.domain.SystemRewardHistory;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import javax.validation.Valid;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.itsource.pojo.query.PageQueryWrapper;
import cn.itsource.result.R;
import cn.itsource.result.PageResult;

@Tag(name = "系统奖励",description = "系统奖励")
@RestController
@RequestMapping("/manager/systemRewardHistory")
public class SystemRewardHistoryController{

    @Autowired
    public ISystemRewardHistoryService systemRewardHistoryService;

    @Operation( summary= "保存SystemRewardHistory",description = "基础对象保存接口")
    @Parameter(name = "systemRewardHistory",description = "保存的对象",required = true)
    @PostMapping
    public R save(@RequestBody @Valid SystemRewardHistory systemRewardHistory){
        return R.success(systemRewardHistoryService.save(systemRewardHistory));
    }

    @Operation( summary= "修改SystemRewardHistory",description = "基础对象修改接口")
    @Parameter(name = "systemRewardHistory",description = "修改的对象",required = true)
    @PutMapping
    public R update(@RequestBody  @Valid SystemRewardHistory systemRewardHistory){
        return R.success(systemRewardHistoryService.updateById(systemRewardHistory));
    }

    //删除对象
    @Operation( summary= "删除SystemRewardHistory",description = "基础对象删除接口，采用状态删除")
    @Parameter(name = "id",description = "删除的对象ID",required = true)
    @DeleteMapping(value="/{id}")
    public R delete(@PathVariable("id") Long id){
        return R.success(systemRewardHistoryService.removeById(id));
    }

    //获取对象
    @Operation( summary= "获取SystemRewardHistory",description = "基础对象获取接口")
    @Parameter(name = "id",description = "查询的对象ID",required = true)
    @GetMapping(value = "/{id}")
    public R get(@PathVariable("id")Long id){
        return R.success(systemRewardHistoryService.getById(id));
    }

    //获取列表:PageQueryWrapper作为通用的查询对象
    @Operation( summary= "查询SystemRewardHistory列表",description = "基础对象列表查询，不带分页")
    @Parameter(name = "query",description = "查询条件",required = true)
    @PostMapping(value = "/list")
    public R list(@RequestBody PageQueryWrapper<SystemRewardHistory> query){
        QueryWrapper<SystemRewardHistory> wrapper = new QueryWrapper<>();
        //实体类作为查询条件
        wrapper.setEntity(query.getQuery());
        return R.success(systemRewardHistoryService.list(wrapper));
    }

    //分页查询
    @Operation( summary= "查询SystemRewardHistory分页列表",description = "基础对象列表查询，带分页")
    @Parameter(name = "query",description = "查询条件，需要指定分页条件",required = true)
    @PostMapping(value = "/pagelist")
    public R page(@RequestBody PageQueryWrapper<SystemRewardHistory> query){
        //分页查询
        Page<SystemRewardHistory> page = new Page<SystemRewardHistory>(query.getPage(),query.getRows());
        QueryWrapper<SystemRewardHistory> wrapper = new QueryWrapper<>();
        //实体类作为查询条件
        wrapper.setEntity(query.getQuery());
        //分页查询
        page = systemRewardHistoryService.page(page,wrapper);
        //返回结果
        return R.success(new PageResult<SystemRewardHistory>(page.getTotal(),page.getRecords()));
    }

}

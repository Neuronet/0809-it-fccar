package cn.itsource.controller.manager;

import cn.itsource.service.IDepartmentService;
import cn.itsource.pojo.domain.Department;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import javax.validation.Valid;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.itsource.pojo.query.PageQueryWrapper;
import cn.itsource.result.R;
import cn.itsource.result.PageResult;

@Tag(name = "",description = "")
@RestController
@RequestMapping("/manager/department")
public class DepartmentController{

    @Autowired
    public IDepartmentService departmentService;

    @Operation( summary= "保存Department",description = "基础对象保存接口")
    @Parameter(name = "department",description = "保存的对象",required = true)
    @PostMapping
    public R save(@RequestBody @Valid Department department){
        return R.success(departmentService.save(department));
    }

    @Operation( summary= "修改Department",description = "基础对象修改接口")
    @Parameter(name = "department",description = "修改的对象",required = true)
    @PutMapping
    public R update(@RequestBody  @Valid Department department){
        return R.success(departmentService.updateById(department));
    }

    //删除对象
    @Operation( summary= "删除Department",description = "基础对象删除接口，采用状态删除")
    @Parameter(name = "id",description = "删除的对象ID",required = true)
    @DeleteMapping(value="/{id}")
    public R delete(@PathVariable("id") Long id){
        return R.success(departmentService.removeById(id));
    }

    //获取对象
    @Operation( summary= "获取Department",description = "基础对象获取接口")
    @Parameter(name = "id",description = "查询的对象ID",required = true)
    @GetMapping(value = "/{id}")
    public R get(@PathVariable("id")Long id){
        return R.success(departmentService.getById(id));
    }

    //获取列表:PageQueryWrapper作为通用的查询对象
    @Operation( summary= "查询Department列表",description = "基础对象列表查询，不带分页")
    @Parameter(name = "query",description = "查询条件",required = true)
    @PostMapping(value = "/list")
    public R list(@RequestBody PageQueryWrapper<Department> query){
        QueryWrapper<Department> wrapper = new QueryWrapper<>();
        //实体类作为查询条件
        wrapper.setEntity(query.getQuery());
        return R.success(departmentService.list(wrapper));
    }

    //分页查询
    @Operation( summary= "查询Department分页列表",description = "基础对象列表查询，带分页")
    @Parameter(name = "query",description = "查询条件，需要指定分页条件",required = true)
    @PostMapping(value = "/pagelist")
    public R page(@RequestBody PageQueryWrapper<Department> query){
        //分页查询
        Page<Department> page = new Page<Department>(query.getPage(),query.getRows());
        QueryWrapper<Department> wrapper = new QueryWrapper<>();
        //实体类作为查询条件
        wrapper.setEntity(query.getQuery());
        //分页查询
        page = departmentService.page(page,wrapper);
        //返回结果
        return R.success(new PageResult<Department>(page.getTotal(),page.getRecords()));
    }

}

package cn.itsource.remote;

import cn.itsource.dto.CreateCustomerFineDto;
import cn.itsource.feign.CustomerFineApi;
import cn.itsource.result.R;
import cn.itsource.service.ICustomerFineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerFineApiRemote implements CustomerFineApi {

    @Autowired
    private ICustomerFineService customerFineService;

    /**
     * 创建司机罚款
     * @param createCustomerFineDto
     * @return
     */
    @Override
    public R createCustomerFine(CreateCustomerFineDto createCustomerFineDto) {
        customerFineService.createCustomerFine(createCustomerFineDto);
        return R.success();
    }
}

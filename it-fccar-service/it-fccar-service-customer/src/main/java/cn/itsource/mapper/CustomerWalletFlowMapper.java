package cn.itsource.mapper;

import cn.itsource.pojo.domain.CustomerWalletFlow;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 钱包流水 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface CustomerWalletFlowMapper extends BaseMapper<CustomerWalletFlow> {

}

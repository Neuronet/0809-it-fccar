package cn.itsource.mapper;

import cn.itsource.pojo.domain.CustomerSummary;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 乘客数据汇总 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface CustomerSummaryMapper extends BaseMapper<CustomerSummary> {

}

package cn.itsource.mapper;

import cn.itsource.pojo.domain.Customer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 客户 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface CustomerMapper extends BaseMapper<Customer> {

}

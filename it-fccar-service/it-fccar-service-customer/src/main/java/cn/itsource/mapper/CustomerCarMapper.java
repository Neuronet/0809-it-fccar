package cn.itsource.mapper;

import cn.itsource.pojo.domain.CustomerCar;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 客户车辆 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface CustomerCarMapper extends BaseMapper<CustomerCar> {

}

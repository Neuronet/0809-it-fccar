package cn.itsource.mapper;

import cn.itsource.pojo.domain.CustomerWallet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 乘客的钱包 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface CustomerWalletMapper extends BaseMapper<CustomerWallet> {

}

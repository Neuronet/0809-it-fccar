package cn.itsource.mapper;

import cn.itsource.pojo.domain.CustomerFine;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 乘客罚款 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface CustomerFineMapper extends BaseMapper<CustomerFine> {

}

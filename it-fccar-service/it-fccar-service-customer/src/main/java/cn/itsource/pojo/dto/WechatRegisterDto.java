package cn.itsource.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class WechatRegisterDto {

    @NotEmpty(message = "参数不能为空！")
    private String openIdCode;

    @NotEmpty(message = "参数不能为空！")
    private String phoneCode;

}

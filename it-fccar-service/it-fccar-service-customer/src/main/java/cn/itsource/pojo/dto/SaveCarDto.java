package cn.itsource.pojo.dto;

import lombok.Data;

@Data
public class SaveCarDto {

    private String carType;
    private String carPlate;

}

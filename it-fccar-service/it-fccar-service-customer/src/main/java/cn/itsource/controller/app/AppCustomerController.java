package cn.itsource.controller.app;

import cn.dev33.satoken.annotation.SaIgnore;
import cn.itsource.pojo.domain.Customer;
import cn.itsource.pojo.dto.SaveCarDto;
import cn.itsource.pojo.dto.WechatRegisterDto;
import cn.itsource.result.R;
import cn.itsource.service.ICustomerCarService;
import cn.itsource.service.ICustomerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Tag(name = "小程序-乘客对象",description = "小程序-乘客对象")
@RestController
@RequestMapping("/app/customer")
public class AppCustomerController {

    @Autowired
    public ICustomerService customerService;

    @Autowired
    private ICustomerCarService customerCarService;

    @Operation( summary= "乘客保存车辆信息",description = "乘客保存车辆信息接口")
    @Parameter(name = "saveCarDto",description = "车辆对象",required = true)
    @PostMapping("/saveCar")
    public R saveCar(@RequestBody @Valid SaveCarDto saveCarDto){
        customerCarService.saveCar(saveCarDto);
        return R.success();
    }

    @Operation( summary= "司机获取车辆信息",description = "司机获取车辆信息接口")
    @GetMapping(value = "/loadCarList")
    public R loadCarList(){
        return R.success(customerCarService.loadCarList());
    }

    @SaIgnore
    @Operation( summary= "乘客微信注册",description = "乘客微信注册接口")
    @Parameter(name = "wechatRegisterDto",description = "微信注册参数对象",required = true)
    @PostMapping("/wechatRegister")
    public R wechatRegister(@RequestBody @Valid WechatRegisterDto wechatRegisterDto){
        customerService.wechatRegister(wechatRegisterDto);
        return R.success();
    }

}

package cn.itsource.service;

import cn.itsource.pojo.domain.CustomerCar;
import cn.itsource.pojo.dto.SaveCarDto;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 客户车辆 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface ICustomerCarService extends IService<CustomerCar> {

    List<CustomerCar> loadCarList();


    void saveCar(SaveCarDto saveCarDto);
}

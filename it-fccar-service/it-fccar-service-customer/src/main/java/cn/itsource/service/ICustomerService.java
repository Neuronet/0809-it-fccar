package cn.itsource.service;

import cn.itsource.pojo.domain.Customer;
import cn.itsource.pojo.dto.SaveCarDto;
import cn.itsource.pojo.dto.WechatRegisterDto;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 客户 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface ICustomerService extends IService<Customer> {

    void wechatRegister(WechatRegisterDto wechatRegisterDto);

}

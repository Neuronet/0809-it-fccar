package cn.itsource.service;

import cn.itsource.pojo.domain.CustomerWallet;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 乘客的钱包 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface ICustomerWalletService extends IService<CustomerWallet> {

}

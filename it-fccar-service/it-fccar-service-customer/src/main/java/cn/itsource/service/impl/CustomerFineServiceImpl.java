package cn.itsource.service.impl;

import cn.itsource.dto.CreateCustomerFineDto;
import cn.itsource.mapper.CustomerFineMapper;
import cn.itsource.pojo.domain.CustomerFine;
import cn.itsource.service.ICustomerFineService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.stereotype.Service;


/**
 * <p>
 * 乘客罚款 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
@Service
public class CustomerFineServiceImpl extends ServiceImpl<CustomerFineMapper, CustomerFine> implements ICustomerFineService {

    /**
     * 创建司机罚款
     * @param createCustomerFineDto
     */
    @Override
    public void createCustomerFine(CreateCustomerFineDto createCustomerFineDto) {
        CustomerFine customerFine = new CustomerFine();
        customerFine.setCustomerId(createCustomerFineDto.getCustomerId());
        customerFine.setOrderId(createCustomerFineDto.getOrderId());
        customerFine.setAmount(createCustomerFineDto.getAmount());
        customerFine.setRemark(createCustomerFineDto.getRemark());
        customerFine.setStatus(1);
        customerFine.setCreateTime(new Date());
        super.save(customerFine);
    }
}

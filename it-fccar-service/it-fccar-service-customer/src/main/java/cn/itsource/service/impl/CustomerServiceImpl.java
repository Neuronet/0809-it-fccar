package cn.itsource.service.impl;

import cn.itsource.constants.Constants;
import cn.itsource.constants.GlobalExceptionCode;
import cn.itsource.feign.LoginApi;
import cn.itsource.mapper.CustomerMapper;
import cn.itsource.pojo.domain.Customer;
import cn.itsource.pojo.domain.CustomerSummary;
import cn.itsource.pojo.domain.CustomerWallet;
import cn.itsource.pojo.dto.LoginDto;
import cn.itsource.pojo.dto.SaveCarDto;
import cn.itsource.pojo.dto.WechatRegisterDto;
import cn.itsource.result.R;
import cn.itsource.service.ICustomerService;
import cn.itsource.service.ICustomerSummaryService;
import cn.itsource.service.ICustomerWalletService;
import cn.itsource.template.WechatTemplate;
import cn.itsource.utils.AssertUtil;
import cn.itsource.utils.BitStatesUtil;
import cn.itsource.utils.NameUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.math.BigDecimal;
import java.util.Date;

import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




/**
 * <p>
 * 客户 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
@Service
public class CustomerServiceImpl extends ServiceImpl<CustomerMapper, Customer> implements ICustomerService {

    @Autowired
    private WechatTemplate wechatTemplate;

    @Autowired
    private ICustomerSummaryService customerSummaryService;

    @Autowired
    private ICustomerWalletService customerWalletService;

    @Autowired
    private LoginApi loginApi;

    /**
     * 乘客微信注册
     * @param wechatRegisterDto
     */
    @Override
    //@GlobalTransactional(name = "customer-wechat-register",rollbackFor = Exception.class)
    public void wechatRegister(WechatRegisterDto wechatRegisterDto) {
        // 1.参数校验-JSR303
        // 2.业务校验
        // 2.1.openId是否能够获取
        String openId = wechatTemplate.code2OpenId(wechatRegisterDto.getOpenIdCode());
        AssertUtil.isNotEmpty(openId, GlobalExceptionCode.WECHAT_REGISTER_ERROR);
        // 2.2.手机号是否能够获取
        String phone = wechatTemplate.code2Phone(wechatRegisterDto.getPhoneCode());
        AssertUtil.isNotEmpty(phone, GlobalExceptionCode.WECHAT_REGISTER_ERROR);
        // 2.3.是否已注册
        Customer customer = super.getOne(new LambdaQueryWrapper<Customer>().eq(Customer::getOpenId, openId));
        AssertUtil.isNull(customer, GlobalExceptionCode.PARAM_PHONE_EXIST);
        // 3.业务实现
        // 3.1.保存乘客信息
        Long id = saveCustomer(phone, openId);
        // 3.2.初始化乘客统计信息
        initCustomerSummary(id);
        // 3.3.初始化乘客钱包信息
        initCustomerWallet(id);
        // 3.4.保存login
        LoginDto loginDto = new LoginDto().setId(id).setPhone(phone).setOpenId(openId).setType(Constants.Login.TYPE_CUSTOMER);
        R result = loginApi.saveLogin(loginDto);
        AssertUtil.isTrue(result.isSuccess(), GlobalExceptionCode.WECHAT_REGISTER_ERROR);
    }


    private void initCustomerWallet(Long id) {
        CustomerWallet customerWallet = new CustomerWallet();
        customerWallet.setId(id);
        customerWallet.setCreateTime(new Date());
        customerWalletService.save(customerWallet);
    }

    private void initCustomerSummary(Long id) {
        CustomerSummary customerSummary = new CustomerSummary();
        customerSummary.setId(id);
        customerSummaryService.save(customerSummary);
    }

    /**
     * 保存customer
     * @param phone
     * @param openId
     * @return
     */
    private Long saveCustomer(String phone, String openId) {
        Customer customer = new Customer();
        customer.setSex(Constants.SEX_MAN);
        customer.setPhone(phone);
        customer.setCreateTime(new Date());
        customer.setOpenId(openId);
        customer.setBitState(BitStatesUtil.OP_PHONE);
        customer.setName(NameUtil.getName());
        customer.setLevel(Constants.Level.LEVEL_BRONZE);
        super.save(customer);
        return customer.getId();
    }
}

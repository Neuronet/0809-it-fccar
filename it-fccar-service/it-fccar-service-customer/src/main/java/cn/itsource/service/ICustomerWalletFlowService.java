package cn.itsource.service;

import cn.itsource.pojo.domain.CustomerWalletFlow;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 钱包流水 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface ICustomerWalletFlowService extends IService<CustomerWalletFlow> {

}

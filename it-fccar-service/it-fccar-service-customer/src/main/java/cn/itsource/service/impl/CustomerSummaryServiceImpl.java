package cn.itsource.service.impl;

import cn.itsource.pojo.domain.CustomerSummary;
import cn.itsource.mapper.CustomerSummaryMapper;
import cn.itsource.service.ICustomerSummaryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 乘客数据汇总 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
@Service
public class CustomerSummaryServiceImpl extends ServiceImpl<CustomerSummaryMapper, CustomerSummary> implements ICustomerSummaryService {

}

package cn.itsource.service.impl;

import cn.itsource.pojo.domain.CustomerWalletFlow;
import cn.itsource.mapper.CustomerWalletFlowMapper;
import cn.itsource.service.ICustomerWalletFlowService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 钱包流水 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
@Service
public class CustomerWalletFlowServiceImpl extends ServiceImpl<CustomerWalletFlowMapper, CustomerWalletFlow> implements ICustomerWalletFlowService {

}

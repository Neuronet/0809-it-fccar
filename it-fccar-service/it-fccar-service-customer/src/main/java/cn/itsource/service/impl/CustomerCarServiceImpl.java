package cn.itsource.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import cn.itsource.constants.GlobalExceptionCode;
import cn.itsource.mapper.CustomerCarMapper;
import cn.itsource.pojo.domain.CustomerCar;
import cn.itsource.pojo.dto.SaveCarDto;
import cn.itsource.service.ICustomerCarService;
import cn.itsource.utils.AssertUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;


/**
 * <p>
 * 客户车辆 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
@Service
public class CustomerCarServiceImpl extends ServiceImpl<CustomerCarMapper, CustomerCar> implements ICustomerCarService {

    /**
     * 保存车辆信息
     * @param saveCarDto
     */
    @Override
    public void saveCar(SaveCarDto saveCarDto) {
        // 1.参数校验-JSR303
        // 2.业务校验
        // 2.1.车辆的车牌号不允许重复
        String carPlate = saveCarDto.getCarPlate();
        String carType = saveCarDto.getCarType();
        long loginId = StpUtil.getLoginIdAsLong();
        CustomerCar customerCar = super.getOne(new LambdaQueryWrapper<CustomerCar>().eq(CustomerCar::getCustomerId, loginId)
                .eq(CustomerCar::getCarPlate, carPlate));
        AssertUtil.isNull(customerCar, GlobalExceptionCode.SERVICE_ERROR);

        // 3.业务实现
        // 3.1.保存乘客车辆信息
        customerCar = new CustomerCar();
        customerCar.setCustomerId(loginId);
        customerCar.setCarPlate(carPlate);
        customerCar.setCarType(carType);
        customerCar.setCreateTime(new Date());
        super.save(customerCar);
    }

    /**
     * 查询当前登录乘客的车辆信息
     * @return
     */
    @Override
    public List<CustomerCar> loadCarList() {
        return super.list(new LambdaQueryWrapper<CustomerCar>().eq(CustomerCar::getCustomerId, StpUtil.getLoginId()));
    }
}

package cn.itsource.service.impl;

import cn.itsource.pojo.domain.CustomerWallet;
import cn.itsource.mapper.CustomerWalletMapper;
import cn.itsource.service.ICustomerWalletService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 乘客的钱包 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
@Service
public class CustomerWalletServiceImpl extends ServiceImpl<CustomerWalletMapper, CustomerWallet> implements ICustomerWalletService {

}

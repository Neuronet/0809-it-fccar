package cn.itsource.service;

import cn.itsource.dto.CreateCustomerFineDto;
import cn.itsource.pojo.domain.CustomerFine;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 乘客罚款 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface ICustomerFineService extends IService<CustomerFine> {

    void createCustomerFine(CreateCustomerFineDto createCustomerFineDto);
}

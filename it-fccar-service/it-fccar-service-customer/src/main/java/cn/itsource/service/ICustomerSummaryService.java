package cn.itsource.service;

import cn.itsource.pojo.domain.CustomerSummary;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 乘客数据汇总 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface ICustomerSummaryService extends IService<CustomerSummary> {

}

package cn.itsource.remote;

import cn.dev33.satoken.annotation.SaIgnore;
import cn.itsource.feign.LoginApi;
import cn.itsource.pojo.dto.LoginDto;
import cn.itsource.result.R;
import cn.itsource.service.ILoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * login接口类
 */
@RestController
public class LoginRemote implements LoginApi {

    @Autowired
    private ILoginService loginService;


    /**
     * 保存login接口
     * @param loginDto
     * @return
     */
    @SaIgnore
    @Override
    public R saveLogin(LoginDto loginDto) {
        loginService.saveLogin(loginDto);
        return R.success();
    }
}

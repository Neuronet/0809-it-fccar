package cn.itsource.service;

import cn.itsource.pojo.domain.Login;
import cn.itsource.pojo.dto.AdminLoginDto;
import cn.itsource.pojo.dto.LoginDto;
import cn.itsource.pojo.vo.LoginInfoVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 登录表 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface ILoginService extends IService<Login> {

    void saveLogin(LoginDto loginDto);

    LoginInfoVo wechatLogin(String openIdCode);

    LoginInfoVo adminLogin(AdminLoginDto adminLoginDto);

    void out();
}

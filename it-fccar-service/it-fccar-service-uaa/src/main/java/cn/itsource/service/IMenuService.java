package cn.itsource.service;

import cn.itsource.pojo.domain.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface IMenuService extends IService<Menu> {

}

package cn.itsource.service;

import cn.itsource.pojo.domain.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface IRoleService extends IService<Role> {

}

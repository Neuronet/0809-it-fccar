package cn.itsource.service;

import cn.itsource.pojo.domain.Permission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface IPermissionService extends IService<Permission> {

}

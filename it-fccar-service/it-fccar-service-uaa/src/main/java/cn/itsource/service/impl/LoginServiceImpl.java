package cn.itsource.service.impl;

import cn.dev33.satoken.secure.BCrypt;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;
import cn.itsource.constants.Constants;
import cn.itsource.constants.GlobalExceptionCode;
import cn.itsource.mapper.PermissionMapper;
import cn.itsource.pojo.domain.Login;
import cn.itsource.mapper.LoginMapper;
import cn.itsource.pojo.dto.AdminLoginDto;
import cn.itsource.pojo.dto.LoginDto;
import cn.itsource.pojo.dto.LoginInfoDto;
import cn.itsource.pojo.vo.LoginInfoVo;
import cn.itsource.service.ILoginService;
import cn.itsource.template.WechatTemplate;
import cn.itsource.utils.AssertUtil;
import cn.itsource.utils.NameUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 登录表 服务实现类
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
@Service
public class LoginServiceImpl extends ServiceImpl<LoginMapper, Login> implements ILoginService {

    @Value("${fccar.default-avatar}")
    private String defaultAvatar;

    @Autowired
    private WechatTemplate wechatTemplate;

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    @Value("${sa-token.permission-key-template}")
    private String permissionKeyTemplate;

    @Value("${sa-token.login-context-info-redis-key}")
    private String loginContextInfoRedisKey;

    /**
     * 保存login
     * @param loginDto
     */
    @Override
    public void saveLogin(LoginDto loginDto) {
        Login login = new Login();
        login.setId(loginDto.getId());
        login.setType(loginDto.getType());
        login.setEnabled(true);
        login.setAvatar(defaultAvatar);
        login.setAdmin(false);
        login.setNickName(NameUtil.getName());
        login.setOpenId(loginDto.getOpenId());
        login.setName(NameUtil.getName());
        login.setPhone(loginDto.getPhone());
        login.setCreateTime(new Date());
        super.save(login);
    }

    /**
     * 微信登录
     * @param openIdCode
     */
    @Override
    public LoginInfoVo wechatLogin(String openIdCode) {
        // 1.参数校验-JSR303
        // 2.业务校验
        // 2.1.openIdCode是否能获取到openId
        String openId = wechatTemplate.code2OpenId(openIdCode);
        AssertUtil.isNotEmpty(openId, GlobalExceptionCode.WECHAT_LOGIN_ERROR);
        // 2.2.openId在数据库中是否存在
        Login login = super.getOne(new LambdaQueryWrapper<Login>().eq(Login::getOpenId, openId));
        AssertUtil.isNotNull(login, GlobalExceptionCode.WECHAT_LOGIN_ERROR);
        // 3.业务实现
        return executeLogin(login);
    }

    private LoginInfoVo executeLogin(Login login) {
        // 3.业务实现 SA-TOKEN  SpringSecurity Vue
        // 3.1.实现登录
        Long loginId = login.getId();
        StpUtil.login(loginId);
        // 3.2.获取token信息
        // 获取当前会话的 token 值
        String tokenValue = StpUtil.getTokenValue();
        // 3.3.获取当前`StpLogic`的 token 名称  拿到的就是 satoken
        String tokenName = StpUtil.getTokenName();
        login.setPassword("");

        // 3.4.获取登录用户权限信息
        Integer type = login.getType();
        LoginInfoVo loginInfoVo = new LoginInfoVo();
        if (type.equals(Constants.Login.TYPE_ADMIN)){
            List<String> permissionSns = permissionMapper.selectPermissionSns(loginId);
            loginInfoVo.setPermissionSns(permissionSns);
            String permissionSnsKey = String.format(permissionKeyTemplate, loginId);
            redisTemplate.opsForValue().set(permissionSnsKey, permissionSns);
        }

        // 3.4.响应信息给前端
        loginInfoVo.setTokenName(tokenName);
        loginInfoVo.setTokenValue(tokenValue);
        loginInfoVo.setLogin(login);

        // 3.5.保存用户信息到Redis，以供后续代码中获取用户信息
        String loginInfoKey = String.format(loginContextInfoRedisKey, loginId);
        LoginInfoDto loginInfoDto = new LoginInfoDto();
        BeanUtil.copyProperties(login, loginInfoDto);
        redisTemplate.opsForValue().set(loginInfoKey, JSONUtil.toJsonStr(loginInfoDto));

        return loginInfoVo;
    }

    /**
     * 后台登录
     * @param adminLoginDto
     * @return
     */
    @Override
    public LoginInfoVo adminLogin(AdminLoginDto adminLoginDto) {
        // 1.参数校验-JSR303
        // 2.业务校验
        // 2.1.校验账号是否存在
        Login login = super.getOne(new LambdaQueryWrapper<Login>().eq(Login::getUsername, adminLoginDto.getUsername()));
        AssertUtil.isNotNull(login, GlobalExceptionCode.WECHAT_LOGIN_ERROR);
        // 2.2.校验密码是否正确
        boolean checkpw = BCrypt.checkpw(adminLoginDto.getPassword(), login.getPassword());
        AssertUtil.isTrue(checkpw, GlobalExceptionCode.WECHAT_LOGIN_ERROR);
        // 3.业务实现
        return executeLogin(login);
    }

    /**
     * 后台退出
     */
    @Override
    public void out() {
        // 1.删除Redis中用户的权限信息
        long loginId = StpUtil.getLoginIdAsLong();
        String permissionSnsKey = String.format(permissionKeyTemplate, loginId);
        redisTemplate.delete(permissionSnsKey);

        // 2.退出登录
        StpUtil.logout();
    }


    public static void main(String[] args) {
//        String pw_hash1 = BCrypt.hashpw("123456", BCrypt.gensalt());
//        String pw_hash2 = BCrypt.hashpw("123456", BCrypt.gensalt());
//        System.out.println(pw_hash1);
//        System.out.println(pw_hash2);

        System.out.println(BCrypt.checkpw("123456", "$2a$10$QO6W/AykmxTMoETofhOqZuZyiLd9KJS99RJk.6XuiBYCoU7AvbbmS"));
        System.out.println(BCrypt.checkpw("123456", "$2a$10$DyXalppHT2QUs6btpF6s8OcTM1Wcvy0gCl886lL3o2yoQZqvrZ5LO"));
    }
}

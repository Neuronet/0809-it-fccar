package cn.itsource.pojo.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("fccar.manager")
public class AdminDefaultLoginInfo {

    private String adminDefaultUsername;
    private String adminDefaultPassword;

}

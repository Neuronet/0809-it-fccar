package cn.itsource.pojo.vo;

import cn.itsource.pojo.domain.Login;
import lombok.Data;

import java.util.List;

@Data
public class LoginInfoVo {

    private String tokenName;

    private String tokenValue;

    private Login login;

    private List<String> permissionSns;

}

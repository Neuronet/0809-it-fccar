package cn.itsource;

import cn.itsource.pojo.domain.Login;
import cn.itsource.service.ILoginService;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import javax.annotation.PostConstruct;

@SpringBootApplication
@EnableDiscoveryClient
public class UaaApp {

    public static void main(String[] args) {
        SpringApplication.run(UaaApp.class, args);
    }

//    @Autowired
//    private ILoginService loginService;

//    @PostConstruct // 此方法在Spring容器初始化完成之后会执行此方法
//    public void initAdmin() {
//        loginService.save(new Login());
//    }

}

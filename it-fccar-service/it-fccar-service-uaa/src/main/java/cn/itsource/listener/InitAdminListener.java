package cn.itsource.listener;

import cn.dev33.satoken.secure.BCrypt;
import cn.hutool.core.util.ObjectUtil;
import cn.itsource.constants.Constants;
import cn.itsource.pojo.domain.Login;
import cn.itsource.pojo.properties.AdminDefaultLoginInfo;
import cn.itsource.service.ILoginService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;


@Component
public class InitAdminListener implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private ILoginService loginService;

    @Value("${fccar.default-avatar}")
    private String defaultAvatar;

    @Autowired
    private AdminDefaultLoginInfo adminDefaultLoginInfo;

    /**
     * 在Spring容器初始化完成之后Bean实例化之后会执行
     * @param event
     */
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        Login login = loginService.getOne(new LambdaQueryWrapper<Login>().eq(Login::getUsername, adminDefaultLoginInfo.getAdminDefaultUsername()));
        // isEmpty：为null的时候为true
        if (ObjectUtil.isEmpty(login)){
            initAdmin();
        }
    }

    private void initAdmin() {
        Login login = new Login();
        login.setUsername(adminDefaultLoginInfo.getAdminDefaultUsername());
        String hashpw = BCrypt.hashpw(adminDefaultLoginInfo.getAdminDefaultPassword(), BCrypt.gensalt());
        login.setPassword(hashpw);
        login.setType(Constants.Login.TYPE_ADMIN);
        login.setEnabled(true);
        login.setAvatar(defaultAvatar);
        login.setAdmin(true);
        login.setNickName("admin");
        login.setName("admin");
        login.setCreateTime(new Date());
        loginService.save(login);
    }
}

package cn.itsource.mapper;

import cn.itsource.pojo.domain.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface PermissionMapper extends BaseMapper<Permission> {

    List<String> selectPermissionSns(Long loginId);
}

package cn.itsource.mapper;

import cn.itsource.pojo.domain.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface MenuMapper extends BaseMapper<Menu> {

}

package cn.itsource.mapper;

import cn.itsource.pojo.domain.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface RoleMapper extends BaseMapper<Role> {

}

package cn.itsource.mapper;

import cn.itsource.pojo.domain.Login;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 登录表 Mapper 接口
 * </p>
 *
 * @author Neuronet
 * @since 2023-12-19
 */
public interface LoginMapper extends BaseMapper<Login> {

}

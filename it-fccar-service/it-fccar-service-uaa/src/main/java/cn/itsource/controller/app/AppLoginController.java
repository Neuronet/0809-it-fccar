package cn.itsource.controller.app;

import cn.dev33.satoken.annotation.SaIgnore;
import cn.itsource.pojo.domain.Login;
import cn.itsource.pojo.vo.LoginInfoVo;
import cn.itsource.result.R;
import cn.itsource.service.ILoginService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

@Tag(name = "小程序登录表",description = "登录表")
@RestController
@RequestMapping("/app/login")
public class AppLoginController {

    @Autowired
    private ILoginService loginService;

    @SaIgnore // 放行
    @Operation( summary= "微信登录",description = "微信登录接口")
    @PostMapping("/wechat/login/{openIdCode}")
    public R wechatLogin(@PathVariable("openIdCode") @NotEmpty(message = "参数异常！") String openIdCode){
        return R.success(loginService.wechatLogin(openIdCode));
    }

}
